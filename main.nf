/**
 * SPDX-FileCopyrightText: 2023 The OTP authors
 *
 * SPDX-License-Identifier: MIT
 *
 * nf-seq-convert
 * A little Nextflow workflow to convert between sequence file formats, such as from gzipped FASTQ to unmapped CRAM.
 *
 * **/
import de.dkfz.odcf.nfSeqConvert.ConfigValidation
import de.dkfz.odcf.nfSeqConvert.Escape

import java.nio.file.Paths

nextflow.enable.dsl=2

// Default parameter values.
params.stageInMode = "rellink"
params.publishMode = "rellink"
params.outputDir = "."


/** Workflow Parameters **/
// Workflow parameters are in JSON/YAML format. We validate the structure according to
// library/src/main/groovy/resources/de/dkfz/odcf/nfSeqConvert/config.schema.json
def configValidator = new ConfigValidation()

if (! workflow.resume) {
    def validationErrors = configValidator.validate(params)
    if (validationErrors.size() > 0) {
        System.err.println("Error validating the configuration: ${validationErrors}")
        exit(1)
    }
}

/** Print the effective parameters, e.g. including the automatically derived values. **/
log.info """
================================
= nf-seq-convert               =
================================
${configValidator.prettyPrint(params)}
"""


String q(String string) {
    return Escape.escapeBash(string)
}

String q(Path path) {
    return Escape.escapeBash(path.toString())
}

String q(Integer i) {
    return i as String
}

defaultMaxBarcodes = -1
defaultReadNumberSuffix = true
defaultAttributes = []
defaultHeaders = []
defaultCommentFormat = "illumina"


/** Workflow Section. **/
// See https://nextflow-io.github.io/patterns/optional-input/
// Note that we have multiple optional `path` parameters. If they use the same NO_FILES, Nextflow
// will complain with "There are multiple input files for each of the following file names: NO_FILES"
// Therefore, a different NO_FILES is used for each parameter.
Map<String, Path> NO_FILES = [
        "input.md5RefFile": projectDir / "assets" / "NO_INPUTMD5REFFILE",
        "output.md5File": projectDir / "assets" / "NO_OUTPUTMD5FILE"
] as Map<String, Path>

process convert {

    errorStrategy {
        // TODO This is only true for LSF. Find a way to discriminate the executor, here.
        // Immediate restart if termination on system-signal (signal number + 128, on LSF)
        // System signals are used to kill the process, if walltime or job memory are exhausted.
        // LSF code = 130 : bkill (SIGINT) => No retry
        (task.exitStatus > 128 && task.exitStatus != 130) ? 'retry' : 'terminate'
    }
    maxRetries 2
    cpus { 2 }
    memory { 400.MB * 2 ** (task.attempt - 1) }
    time { 12.hours * 2 ** (task.attempt - 1) }
    publishDir params.outputDir, mode: params.publishMode
    stageInMode params.stageInMode

    input:
        path inputFile
        val outputFile          // output paths must be provided as strings
        val inputFormat
        val outputFormat
        path inputMd5RefFile    // optional input file
        val inputMd5RefString
        val inputMd5File        // output paths must be provided as strings. This *is* an output file!
        val outputMd5File       // output paths must be provided as strings
        val samHeaders
        val samAttributes
        val readNumber
        val readNumberSuffix
        val readCount
        val maxBarcodes
        val commentFormat

    output:
        tuple   path(inputMd5File),
                path(outputFile),
                path(resultOutputMd5File)

    script:
        inputFormatParam = inputFormat != [] ? "--infile-format ${q(inputFormat.join(","))}" : ""

        inputMd5RefFileParam = inputMd5RefFile.target != NO_FILES.get("input.md5RefFile") ? "--in-md5-ref-file ${q(inputMd5RefFile)}" : ""
        inputMd5RefStringParam = inputMd5RefString != "" ? "--in-md5-ref ${q(inputMd5RefString)}" : ""

        inputMd5FileParam = "--in-md5-file ${q(inputMd5File)}"

        outputFormatParam = outputFormat != [] ? "--outfile-format ${q(outputFormat.join(","))}" : ""
        resultOutputMd5File = Paths.get(outputMd5File) != NO_FILES.get("output.md5File") ? outputMd5File : Paths.get(outputFile + ".md5")
        outputMd5FileParam = "--out-md5-file ${q(resultOutputMd5File)}"

        readNumberParam = readNumber != -1 ? "--read-number ${q(readNumber)}" : ""
        readNumberSuffixParam = readNumberSuffix ? "--read-number-suffix" : ""

        readCountParam = readCount != -1 ? "--read-count ${q(readCount)}" : ""

        commentFormatParam = "--comment-format ${q(commentFormat)}"
        maxBarcodesParam = maxBarcodes >= 0 ? "--max-barcodes ${q(maxBarcodes)}" : ""

        """
        pwd   # Display the work-directory in the log file.

        # Ensure all (relative) output directories exist.
        mkdir -p \$(dirname ${q(outputFile)})
        mkdir -p \$(dirname ${q(resultOutputMd5File)})
        mkdir -p \$(dirname ${q(inputMd5File)})

        seqConvert.py \
            --infile ${q(inputFile)} \
            ${inputFormatParam} \
            ${inputMd5RefFileParam} \
            ${inputMd5RefStringParam} \
            ${inputMd5FileParam} \
            --outfile ${q(outputFile)} \
            ${outputFormatParam} \
            ${outputMd5FileParam} \
            ${samHeaders.collect {"--sam-header ${q(it)}"}.join(" ") } \
            ${samAttributes.collect {"--sam-attributes ${q(it)}"}.join(" ") } \
            ${readNumberParam} \
            ${readNumberSuffixParam} \
            ${readCountParam} \
            ${commentFormatParam} \
            ${maxBarcodesParam}
        """
}


workflow seqConvert {
    take:
        inputFile
        outputFile
        inputFormat
        outputFormat
        inputMd5RefFile
        inputMd5RefString
        inputMd5File
        outputMd5File
        samHeaders
        samAttributes
        readNumber
        readNumberSuffix
        readCount
        maxBarcodes
        commentFormat
    main:
        convert(inputFile,
                outputFile,
                inputFormat,
                outputFormat,
                inputMd5RefFile,
                inputMd5RefString,
                inputMd5File,
                outputMd5File,
                samHeaders,
                samAttributes,
                readNumber,
                readNumberSuffix,
                readCount,
                maxBarcodes,
                commentFormat)
    emit:
        convert.out
}

workflow {
    conversionSpecs = Channel.fromList(params.conversions)
    seqConvert(
            conversionSpecs.map {
                Paths.get(it["infile"]["path"])
            },
            conversionSpecs.map {
                // Output filenames must be provided as strings, not as paths.
                it["outfile"]["path"]
            },
            conversionSpecs.map { Optional.ofNullable(it["infile"]["format"]).orElse([]) },
            conversionSpecs.map { Optional.ofNullable(it["outfile"]["format"]).orElse([]) },
            conversionSpecs.map {
                Optional.ofNullable(it["infile"]["md5RefPath"]).
                        map { Paths.get(it) }.
                        orElse(NO_FILES.get("input.md5RefFile"))
            },
            conversionSpecs.map {
                Optional.ofNullable(it["infile"]["md5RefString"]).orElse("")
            },
            conversionSpecs.map {
                Optional.ofNullable(it["infile"]["md5Path"]).
                        orElse(it["outfile"]["path"] + ".source_md5")
            },
            conversionSpecs.map {
                Optional.ofNullable(it["outfile"]["md5Path"]).
                        orElse(NO_FILES.get("output.md5File").toString())
            },
            conversionSpecs.map {
                Optional.ofNullable(it["outfile"]["headers"]).
                    orElse(defaultHeaders)
            },
            conversionSpecs.map {
                Optional.ofNullable(it["outfile"]["attributes"]).
                        orElse(defaultAttributes)
            },
            conversionSpecs.map {
                Optional.ofNullable(it["infile"]["readNumber"]).
                        orElse(-1)
            },
            conversionSpecs.map {
                Optional.ofNullable(it["infile"]["readNumberSuffix"]).
                        orElse(defaultReadNumberSuffix)
            },
            conversionSpecs.map {
                Optional.ofNullable(it["infile"]["readCount"]).
                        orElse(-1)
            },
            conversionSpecs.map {
                Optional.ofNullable(it["infile"]["maxBarcodes"]).
                        orElse(defaultMaxBarcodes)
            },
            conversionSpecs.map {
                Optional.ofNullable(it["outfile"]["commentFormat"]).
                        orElse(defaultCommentFormat)
            },
    )
}

workflow.onComplete {
    println "Workflow run $workflow.runName completed at $workflow.complete with status " +
            "${workflow.success ? 'success' : 'failure'}"
}
