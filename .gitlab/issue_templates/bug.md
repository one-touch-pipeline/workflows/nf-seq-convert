<!--
SPDX-FileCopyrightText: 2023 The OTP authors

SPDX-License-Identifier: MIT
-->

\label ~new

## Observed behaviour

## Expected behaviour

## Impact

<!-- 
- Who is affect? E.g.
  - stakeholder groups, projects
  - refrain from adding confidential information
- How much is the stakeholder affected?
  - Can the error be overlooked by the user? (e.g. fatal errors cannot be overlooked)
  - Will urgent analyses be delayed? Why is the analysis urgent?
- How will the error affect the results?
  - In particular, is there a chance of considerable misinterpretation of data?
-->

## Reproducing the error

<!--
Detailed description of how the error can be reproduced.
-->