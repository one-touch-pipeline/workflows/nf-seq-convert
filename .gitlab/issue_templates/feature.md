<!--
SPDX-FileCopyrightText: 2023 The OTP authors

SPDX-License-Identifier: MIT
-->

/label ~new

<!-- 
Modify the content of the issue. The description should describe the final state of the feature that shall be implemented. As the discussion on this issue progresses, the description will have to be updated.
-->

## User Story

<!-- 
Describe the feature to be implemented. Refrain from including statements about the implementation of the feature. The story must express, why the feature is needed, who needs it (roles), and what exactly the feature should do.
-->

### Why?

### Who?

### What?

<!-- Nope, no "How?"! -->

## Technical Notes

<!--
Describe technical information that will be important to decide on the implementation of the
feature. 

You may make **suggestions** about how the feature could be implemented. The decision, however, is to the developer(s).
-->
