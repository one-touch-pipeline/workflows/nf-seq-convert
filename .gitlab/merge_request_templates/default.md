<!--
SPDX-FileCopyrightText: 2023 The OTP authors

SPDX-License-Identifier: MIT
-->

## Changelog

<!-- concisely summarize the changes -->

FILL IN

<!-- Fill in the associated issue number! -->
Closes FILL IN ISSUE

## For the author and reviewer(s)

* [ ] Merge request title concisely describes the changes.
* [ ] Manual verification of the results was done.
* [ ] Changes are described in the change-log file and classified there according to [semantic versioning](#SemanticVersioning) into "major", "minor", "patch" levels.
* [ ] The tests cover corner cases (empty files, syntax errors in input, I/O errors, etc.).
* [ ] The code is readable and follows the DRY principle.
* [ ] Deactivation of automatic checks (e.g. flake8, mypy) is justified in comments in the code.

Please describe all your findings as comments to this merge-request thread (below).
