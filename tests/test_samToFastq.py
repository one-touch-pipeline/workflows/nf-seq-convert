# SPDX-FileCopyrightText: 2023 The OTP authors
#
# SPDX-License-Identifier: MIT
from collections import defaultdict
from io import StringIO
from pathlib import Path
from textwrap import dedent

import pytest

from lib import SeqEntry, IlluminaComment, CommentFormat, OpaqueComment
from samToFastq import (is_sam_header_line, parse_sam_line, parse_tags,
                        main, extract_flag_information, create_comment, parse_args)


def test_extract_flag_information():
    assert extract_flag_information(0) == (None, 1, False)
    assert extract_flag_information(577) == (1, 1, True)
    assert extract_flag_information(641) == (2, 2, True)
    assert extract_flag_information(4019) == (2, 2, True)

    with pytest.raises(RuntimeError):
        _ = extract_flag_information(705)


def test_create_comment():
    assert create_comment(comment_format=CommentFormat.OPAQUE,
                          comment_read_number=1,
                          is_filtered=False,
                          opaque_comment="blabla",
                          barcode="",
                          control_bits=0) == OpaqueComment.create("blabla")
    assert create_comment(comment_format=CommentFormat.ILLUMINA,
                          comment_read_number=2,
                          is_filtered=True,
                          opaque_comment="blabla",
                          barcode="ABC",
                          control_bits=0) == IlluminaComment.create("2:Y:0:ABC")


def test_is_sam_header_line():
    assert not is_sam_header_line("")
    seq_line = "\t".join([
        "HWI-ST791:134804979:D1VCPACXX:1:1108:11265:85901",
        "141", "*", "0", "0", "*", "*", "0", "0",
        "AGGGTTAGGGTTGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGG"
        "GTTAGGGTTAGGGGTAGGGTTAGGGTTAGGGTTAGGGTTAGGGGGAGGGG",
        "?@@<ADDDHFAD:G<2<ABEH?F9EHEAB:DG>*98B>F/?0?BC=8=FD"
        ";)6=CEE###########################################"
    ])
    assert not is_sam_header_line(seq_line)
    assert is_sam_header_line("\t".join([
        "@PG", "ID:fastqToSam.py", "PN:fastqToSam.py",
        "CL:fastqToSam.py --fastq-in /dev/stdin --sam-out /dev/stdout"]))
    assert is_sam_header_line("\t".join(["@RG", "ID:1", "DS:bla"]))
    assert is_sam_header_line("\t".join(["@CO", "whetever comment is here"]))


def test_parse_tags():
    assert parse_tags([]) == {}

    assert parse_tags([
        "CO:Z:a", "CO:Z:b"
    ]) == defaultdict(set, {
        "CO": {"a", "b"}
    })

    assert parse_tags([
        "CO:Z:a", "XF:Z:b", "BC:i:0", "CO:Z:d"
    ]) == defaultdict(set, {
        "CO": {"a", "d"},
        "XF": {"b"},
        "BC": {"0"}
    })


def test_parse_sam_line():
    # Paired read with no opaque comment.
    line1 = "\t".join([
        "HWI-ST791:134804979:D1VCPACXX:1:1108:11265:85901",
        "141", "*", "0", "0", "*", "*", "0", "0",
        "AGGGTTAGGGTTGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGG"
        "GTTAGGGTTAGGGGTAGGGTTAGGGTTAGGGTTAGGGTTAGGGGGAGGGG",
        "?@@<ADDDHFAD:G<2<ABEH?F9EHEAB:DG>*98B>F/?0?BC=8=FD"
        ";)6=CEE###########################################"
    ])
    assert parse_sam_line(line1, comment_format=CommentFormat.OPAQUE) == \
        SeqEntry[OpaqueComment](
            identifier="HWI-ST791:134804979:D1VCPACXX:1:1108:11265:85901",
            sequence="AGGGTTAGGGTTGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGGGTTAGG"
                     "GTTAGGGTTAGGGGTAGGGTTAGGGTTAGGGTTAGGGTTAGGGGGAGGGG",
            qualities="?@@<ADDDHFAD:G<2<ABEH?F9EHEAB:DG>*98B>F/?0?BC=8=F"
                      "D;)6=CEE###########################################",
            read_number=2,
            comment=None
        )

    # Non-paired read with tag-derived IlluminaComment
    line2 = "\t".join([
        "HWI-ST791:134804979:D1VCPACXX:1:1106:7277:51314",
        "516", "*", "0", "0", "*", "*", "0", "0",
        "AACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAA"
        "CCCTAACCCTAACCCTAACACTAACCCTGACCCTAACCCTCACCCGAACC",
        "@@@BDDDDFD??FHGGIGGGIIBDCHGGAFGHGGGE@B?;89?BHCG@BC"
        "=CA68.=6@DAAAC####################################",
        "RG:Z:1", "BC:Z:ACTG", "XC:i:1"])
    assert parse_sam_line(line2, comment_format=CommentFormat.ILLUMINA) == \
           SeqEntry[IlluminaComment](
               identifier="HWI-ST791:134804979:D1VCPACXX:1:1106:7277:51314",
               sequence="AACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAA"
                        "CCCTAACCCTAACCCTAACACTAACCCTGACCCTAACCCTCACCCGAACC",
               qualities="@@@BDDDDFD??FHGGIGGGIIBDCHGGAFGHGGGE@B?;89?BHCG@B"
                         "C=CA68.=6@DAAAC####################################",
               read_number=None,
               comment=IlluminaComment.create("1:Y:1:ACTG")
           )

    # Non-paired read with default IlluminaComment.
    line3 = "\t".join([
        "HWI-ST791:134804979:D1VCPACXX:1:1106:7277:51314",
        "516", "*", "0", "0", "*", "*", "0", "0",
        "AACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAA"
        "CCCTAACCCTAACCCTAACACTAACCCTGACCCTAACCCTCACCCGAACC",
        "@@@BDDDDFD??FHGGIGGGIIBDCHGGAFGHGGGE@B?;89?BHCG@BC"
        "=CA68.=6@DAAAC####################################"])
    assert parse_sam_line(line3, comment_format=CommentFormat.ILLUMINA) == \
           SeqEntry[IlluminaComment](
               identifier="HWI-ST791:134804979:D1VCPACXX:1:1106:7277:51314",
               sequence="AACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAA"
                        "CCCTAACCCTAACCCTAACACTAACCCTGACCCTAACCCTCACCCGAACC",
               qualities="@@@BDDDDFD??FHGGIGGGIIBDCHGGAFGHGGGE@B?;89?BHCG@B"
                         "C=CA68.=6@DAAAC####################################",
               read_number=None,
               comment=IlluminaComment.create("1:Y:0:")
           )

    # Too short line
    line4 = "\t".join([
        "HWI-ST791:134804979:D1VCPACXX:1:1106:7277:51314",
        "516", "*", "0", "0", "*", "*", "0", "0",
        "AACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAA"
        "CCCTAACCCTAACCCTAACACTAACCCTGACCCTAACCCTCACCCGAACC"])
    with pytest.raises(ValueError):
        _ = parse_sam_line(line4, comment_format=CommentFormat.ILLUMINA)

    # Non-integer flags field
    line5 = "\t".join([
        "HWI-ST791:134804979:D1VCPACXX:1:1106:7277:51314",
        "XXX", "*", "0", "0", "*", "*", "0", "0",
        "AACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAA"
        "CCCTAACCCTAACCCTAACACTAACCCTGACCCTAACCCTCACCCGAACC",
        "@@@BDDDDFD??FHGGIGGGIIBDCHGGAFGHGGGE@B?;89?BHCG@BC"
        "=CA68.=6@DAAAC####################################"])
    with pytest.raises(ValueError):
        _ = parse_sam_line(line5, comment_format=CommentFormat.ILLUMINA)

    # Qualities and sequence have different length
    line6 = "\t".join([
        "HWI-ST791:134804979:D1VCPACXX:1:1106:7277:51314",
        "516", "*", "0", "0", "*", "*", "0", "0",
        "AACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAA"
        "CCCTAACCCTAACCCTAACACTAACCCTGACCCTAACCCTCACCCGAACC",
        "@@@BDDDDFD??FHGGIGGGIIBDCHGGAFGHGGGE@B?;89?BHCG@BC"
        "=CA68.=6@DAAAC##########################"])
    with pytest.raises(ValueError):
        _ = parse_sam_line(line6, comment_format=CommentFormat.ILLUMINA)


def test_main():
    empty_sam = ""
    in_fh = StringIO(empty_sam)
    empty_fh = StringIO("")
    main(in_fh,
         empty_fh,
         CommentFormat.OPAQUE,
         expected_read_count=0,
         read_number_suffix=True)
    assert "" == empty_fh.getvalue()

    non_empty_sam = "\n".join([
        "\t".join(["@RG", "ID:1", "DS:bla"]),
        "\t".join([
            "HWI-ST791:134804979:D1VCPACXX:1:1106:7277:51314",
            "4", "*", "0", "0", "*", "*", "0", "0",
            "AACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAA"
            "CCCTAACCCTAACCCTAACACTAACCCTGACCCTAACCCTCACCCGAACC",
            "@@@BDDDDFD??FHGGIGGGIIBDCHGGAFGHGGGE@B?;89?BHCG@BC"
            "=CA68.=6@DAAAC####################################",
            "RG:Z:1", "XF:Z:the comment"]),
        "\t".join([
            "HWI-ST791:134804979:D1VCPACXX:1:1106:7277:51314",
            "645", "*", "0", "0", "*", "*", "0", "0",
            "AACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAA"
            "CCCTAACCCTAACCCTAACACTAACCCTGACCCTAACCCTCACCCGAACC",
            "@@@BDDDDFD??FHGGIGGGIIBDCHGGAFGHGGGE@B?;89?BHCG@BC"
            "=CA68.=6@DAAAC####################################",
            "RG:Z:1", "BC:Z:ACTG", "XC:i:1"])
        ]) + "\n"

    in_fh = StringIO(non_empty_sam)
    opaque_comment_fh = StringIO("")
    main(in_fh,
         opaque_comment_fh,
         CommentFormat.OPAQUE,
         expected_read_count=2,
         read_number_suffix=True)
    opaque_comment_res = dedent("""\
        @HWI-ST791:134804979:D1VCPACXX:1:1106:7277:51314 the comment
        AACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACACTAACCCTGACCCTAACCCTCACCCGAACC
        +
        @@@BDDDDFD??FHGGIGGGIIBDCHGGAFGHGGGE@B?;89?BHCG@BC=CA68.=6@DAAAC####################################
        @HWI-ST791:134804979:D1VCPACXX:1:1106:7277:51314/2
        AACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACACTAACCCTGACCCTAACCCTCACCCGAACC
        +
        @@@BDDDDFD??FHGGIGGGIIBDCHGGAFGHGGGE@B?;89?BHCG@BC=CA68.=6@DAAAC####################################
        """)
    assert opaque_comment_fh.getvalue() == opaque_comment_res, "Opaque comment output"

    in_fh = StringIO(non_empty_sam)
    illumina_comment_fh = StringIO("")
    main(in_fh,
         illumina_comment_fh,
         CommentFormat.ILLUMINA,
         expected_read_count=2,
         read_number_suffix=True)
    illumina_comment_res = dedent("""\
        @HWI-ST791:134804979:D1VCPACXX:1:1106:7277:51314 1:N:0:
        AACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACACTAACCCTGACCCTAACCCTCACCCGAACC
        +
        @@@BDDDDFD??FHGGIGGGIIBDCHGGAFGHGGGE@B?;89?BHCG@BC=CA68.=6@DAAAC####################################
        @HWI-ST791:134804979:D1VCPACXX:1:1106:7277:51314/2 2:Y:1:ACTG
        AACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACCCTAACACTAACCCTGACCCTAACCCTCACCCGAACC
        +
        @@@BDDDDFD??FHGGIGGGIIBDCHGGAFGHGGGE@B?;89?BHCG@BC=CA68.=6@DAAAC####################################
        """)
    assert illumina_comment_fh.getvalue() == illumina_comment_res, "Illumina comment output"

    with pytest.raises(ValueError):
        in_fh = StringIO(non_empty_sam + non_empty_sam + non_empty_sam)
        out_fh = StringIO("")
        main(in_fh,
             out_fh,
             CommentFormat.ILLUMINA,
             expected_read_count=5,   # should be 6
             read_number_suffix=True)


def test_pargs_args():
    r1 = parse_args(["-i", "infile",
                     "-o", "outfile",
                     "-k", "opaque",
                     "-s",
                     "-C", "100"
                     ])
    assert r1.sam == Path("infile")
    assert r1.fastq == Path("outfile")
    assert r1.comment_format == CommentFormat.OPAQUE
    assert r1.read_count == 100
    assert r1.read_number_suffix

    r2 = parse_args(["-i", "infile",
                     "-o", "outfile",
                     ])
    assert r2.sam == Path("infile")
    assert r2.fastq == Path("outfile")
    assert r2.comment_format == CommentFormat.ILLUMINA
    assert r2.read_count is None
    assert not r2.read_number_suffix

    r3 = parse_args(["--sam-in", "infile",
                     "--fastq-out", "outfile",
                     "--comment-format", "opaque",
                     "--read-number-suffix",
                     "--read-count", "100"
                     ])
    assert r3.sam == Path("infile")
    assert r3.fastq == Path("outfile")
    assert r3.comment_format == CommentFormat.OPAQUE
    assert r3.read_count == 100
    assert r3.read_number_suffix
