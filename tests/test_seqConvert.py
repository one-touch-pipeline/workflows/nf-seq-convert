# SPDX-FileCopyrightText: 2023 The OTP authors
#
# SPDX-License-Identifier: MIT
from argparse import Namespace
from pathlib import Path
from typing import Union, Dict, List

import pytest

from lib import CommentFormat
from seqConvert import \
    Format, MapSpec, to_map_spec, unpack_command, parse_format_spec, \
    pack_command, conversion_command, InputFileSpec, OutputFileSpec, parse_args


def test_format_from_spec_name():
    assert Format.from_spec_name("FASTQ") == Format.FASTQ
    assert Format.from_spec_name("UNMAPPED_BAM") == Format.UNMAPPED_BAM
    assert Format.from_spec_name("UNMAPPED_TAM") == Format.UNMAPPED_TAM
    assert Format.from_spec_name("UNMAPPED_CRAM") == Format.UNMAPPED_CRAM
    assert Format.from_spec_name("GZIP") == Format.GZIP
    assert Format.from_spec_name("BZIP2") == Format.BZIP2

    with pytest.raises(ValueError):
        Format.from_spec_name("FAIL")


def test_from_suffixe():
    assert Format.from_suffix(".fastq") == [Format.FASTQ]
    assert Format.from_suffix("fq") == [Format.FASTQ]
    assert Format.from_suffix(".gz") == [Format.GZIP]
    assert Format.from_suffix(".bz2") == [Format.BZIP2]
    assert Format.from_suffix("bam") == [Format.UNMAPPED_BAM]
    assert Format.from_suffix("sam") == [Format.UNMAPPED_TAM]
    assert Format.from_suffix(".cram") == [Format.UNMAPPED_CRAM]
    assert Format.from_suffix("ubam") == [Format.UNMAPPED_BAM]
    assert Format.from_suffix("usam") == [Format.UNMAPPED_TAM]
    assert Format.from_suffix(".ucram") == [Format.UNMAPPED_CRAM]

    with pytest.raises(ValueError):
        Format.from_suffix(".fail")


def test_from_suffixes():
    assert Format.from_suffixes(Path("/a/b/c.fastq.gz")) == \
           (Path("/a/b/c"), [Format.FASTQ, Format.GZIP])
    assert Format.from_suffixes(Path("/a/b/c.fastq.bz2")) == \
           (Path("/a/b/c"), [Format.FASTQ, Format.BZIP2])
    assert Format.from_suffixes(Path("/b/d.x.sam.bz2")) == \
           (Path("/b/d.x"), [Format.UNMAPPED_TAM, Format.BZIP2])
    assert Format.from_suffixes(Path("/1/2.sorted.usam.bz2")) == \
           (Path("/1/2.sorted"), [Format.UNMAPPED_TAM, Format.BZIP2])
    assert Format.from_suffixes(Path("/1/2.sorted.bam.bz2")) == \
           (Path("/1/2.sorted"), [Format.UNMAPPED_BAM, Format.BZIP2])

    # A hidden file
    assert Format.from_suffixes(Path(".sorted.bam.bz2")) == \
           (Path(".sorted"), [Format.UNMAPPED_BAM, Format.BZIP2])

    with pytest.raises(ValueError):
        # This file has .bam as filename base, not as suffix, and is therefore interpreted as
        # hidden, not as BAM+Bzip2.
        Format.from_suffixes(Path(".bam.bz2"))

    with pytest.raises(ValueError):
        # Automatically derived innermost format is not sequence/alignment format.
        Format.from_suffixes(Path("/a/b.gz.bzip2"))


def test_parse_format_spec():
    assert parse_format_spec("FASTQ,GZIP") == [Format.FASTQ, Format.GZIP]
    assert parse_format_spec("UNMAPPED_TAM,BZIP2") == [Format.UNMAPPED_TAM, Format.BZIP2]


def args(**kwargs) -> Namespace:
    defaults: Dict[str, Union[str, None, bool, int, List[str], CommentFormat]] = {
        # "infile":
        "in_md5_ref_string": None,
        "in_md5_ref_file": None,
        "in_md5_file": None,
        "read_count": None,
        "read_number_suffix": False,
        "comment_format": CommentFormat.ILLUMINA,
        "infile_format": None,
        # "outfile":
        "outfile_format": None,
        "out_md5_file": None,
        "max_barcodes": None,
        "sam_attributes": [],
        "sam_headers": [],
        "read_number": None
    }
    return Namespace(**{**defaults, **kwargs})


def test_to_map_spec():
    t1 = to_map_spec(args(infile=Path("/a/a.fastq"),
                          outfile=Path("some.fastq.gz")))
    assert t1.input.formats == [Format.FASTQ]
    assert t1.output.formats == [Format.FASTQ, Format.GZIP]

    t2 = to_map_spec(args(infile=Path("/a/a.cram"),
                          outfile=Path("some.fastq.gz")))
    assert t2.input.formats == [Format.UNMAPPED_CRAM]
    assert t2.output.formats == [Format.FASTQ, Format.GZIP]

    t3 = to_map_spec(args(infile=Path("/a/a.something"),
                          infile_format=[Format.UNMAPPED_CRAM],
                          outfile=Path("some.fastq.gz"),
                          outfile_format=[Format.FASTQ, Format.BZIP2]))
    assert t3.input.formats == [Format.UNMAPPED_CRAM]
    assert t3.output.formats == [Format.FASTQ, Format.BZIP2]
    assert t3.input.md5_ref is None
    assert t3.input.md5_file == Path("some.fastq.gz.source_md5")
    assert t3.output.md5_file == Path("some.fastq.gz.md5")

    t4 = to_map_spec(args(infile=Path("/a/a.something"),
                          in_md5_file=Path("a.something.input_md5"),
                          infile_format=[Format.UNMAPPED_CRAM],
                          outfile=Path("some.fastq.gz"),
                          out_md5_file=Path("some.fastq.gz.output_md5"),
                          outfile_format=[Format.FASTQ, Format.BZIP2]))
    assert t4.input.md5_file == Path("a.something.input_md5")
    assert t4.output.md5_file == Path("some.fastq.gz.output_md5")

    with pytest.raises(RuntimeError):
        # Fail inferring type from filename.
        _ = to_map_spec(args(infile=Path("/a/a.cram"),
                             outfile=Path("some.nothing")))

    with pytest.raises(ValueError):
        # Both MD5 file and checksum
        _ = to_map_spec(args(infile=Path("/a/a.cram"),
                             in_md5_ref_file=Path("a.something.input_md5"),
                             in_md5_ref_string="3bd0587bdfebeb572e2acf08539cb053",
                             outfile=Path("some.fastq.gz")))

    with pytest.raises(ValueError):
        # Fail because of incorrect MD5 string
        _ = to_map_spec(args(infile=Path("/a/a.cram"),
                             in_md5_ref_string="not an MD5, sorry",
                             outfile=Path("/b/b.fastq")))


def test_unpack_command():
    assert unpack_command(InputFileSpec(file=Path("a/b/c"),
                                        md5_file=Path("/path/to.md5"),
                                        formats=[Format.FASTQ])) == \
        ["cat a/b/c",
         "calcMd5 -f c -o /path/to.md5",
         "(cat /dev/stdin > /dev/stdout)"]
    assert unpack_command(InputFileSpec(file=Path("a/b/c"),
                                        md5_ref=Path("/path/from.md5"),
                                        md5_file=Path("/path/to.md5"),
                                        formats=[Format.FASTQ, Format.GZIP])) == \
           ["cat a/b/c",
            "calcMd5 -f c -o /path/to.md5 -e /path/from.md5",
            "gunzip -c /dev/stdin > /dev/stdout",
            "(cat /dev/stdin > /dev/stdout)"]
    assert unpack_command(InputFileSpec(Path("a/b/c"),
                                        md5_file=Path("/path/to.md5"),
                                        formats=[Format.UNMAPPED_TAM, Format.BZIP2])) == \
           ["cat a/b/c",
            "calcMd5 -f c -o /path/to.md5",
            "bunzip2 -c /dev/stdin > /dev/stdout",
            "samToFastq.py --sam-in /dev/stdin --fastq-out /dev/stdout --comment-format illumina"]
    assert unpack_command(InputFileSpec(file=Path("a/b/c"),
                                        formats=[Format.UNMAPPED_BAM],
                                        comment_format=CommentFormat.OPAQUE)) == \
           ["cat a/b/c",
            "samtools view -h /dev/stdin | "
            "samToFastq.py --sam-in /dev/stdin --fastq-out /dev/stdout --comment-format opaque"]
    assert unpack_command(InputFileSpec(Path("a/b/c"),
                                        md5_file=Path("/path/to.md5"),
                                        formats=[Format.UNMAPPED_CRAM],
                                        comment_format=CommentFormat.ILLUMINA,
                                        read_number_suffix=True)) == \
           ["cat a/b/c",
            "calcMd5 -f c -o /path/to.md5",
            "samtools view -h /dev/stdin | "
            "samToFastq.py --sam-in /dev/stdin --fastq-out /dev/stdout "
            "--comment-format illumina --read-number-suffix"]
    assert unpack_command(InputFileSpec(file=Path("a/b/c"),
                                        md5_ref="3bd0587bdfebeb572e2acf08539cb053",
                                        md5_file=Path("/path/to.md5"),
                                        formats=[Format.FASTQ, Format.GZIP])) == \
           ["cat a/b/c",
            "calcMd5 -f c -o /path/to.md5 -E 3bd0587bdfebeb572e2acf08539cb053",
            "gunzip -c /dev/stdin > /dev/stdout",
            "(cat /dev/stdin > /dev/stdout)"]


def test_pack_command():
    assert pack_command(OutputFileSpec(file=Path("a/b/c"),
                                       md5_file=Path("/path/to.md5"),
                                       formats=[Format.FASTQ])) == \
           ["(cat /dev/stdin > /dev/stdout)",
            "calcMd5 -f c -o /path/to.md5 > a/b/c"]
    assert pack_command(OutputFileSpec(Path("a/b/c"),
                                       md5_file=Path("/path/to.md5"),
                                       formats=[Format.FASTQ, Format.GZIP])) == \
           ["(cat /dev/stdin > /dev/stdout)",
            "gzip -c /dev/stdin > /dev/stdout",
            "calcMd5 -f c -o /path/to.md5 > a/b/c"]
    assert pack_command(OutputFileSpec(Path("a/b/c"),
                                       md5_file=Path("/path/to.md5"),
                                       formats=[Format.UNMAPPED_TAM, Format.BZIP2],
                                       sam_attributes=["XC:Z:hallo1", "XI:I:1"])) == \
           ["fastqToSam.py --fastq-in /dev/stdin --sam-out /dev/stdout "
            "--comment-format illumina --attribute XC:Z:hallo1 --attribute XI:I:1",
            "bzip2 -c /dev/stdin > /dev/stdout",
            "calcMd5 -f c -o /path/to.md5 > a/b/c"]
    assert pack_command(OutputFileSpec(file=Path("a/b/c"),
                                       md5_file=Path("/path/to.md5"),
                                       formats=[Format.UNMAPPED_BAM],
                                       comment_format=CommentFormat.OPAQUE,
                                       read_count=100,
                                       read_number=1,
                                       sam_headers=["@RG\tblabla"])) == \
           ["fastqToSam.py --fastq-in /dev/stdin --comment-format opaque --header '@RG\tblabla' "
            "--read-number 1 --read-count 100 | "
            "samtools view -b -o /dev/stdout",
            "calcMd5 -f c -o /path/to.md5 > a/b/c"
            ]
    assert pack_command(OutputFileSpec(file=Path("a/b/c"),
                                       md5_file=Path("/path/to.md5"),
                                       formats=[Format.UNMAPPED_CRAM],
                                       max_barcodes=10)) == \
           ["fastqToSam.py --fastq-in /dev/stdin --max-barcodes 10 --comment-format illumina | "
            "samtools view --output-fmt cram,embed_ref=1 -o /dev/stdout",
            "calcMd5 -f c -o /path/to.md5 > a/b/c"]


def test_conversion_command():
    assert conversion_command(MapSpec(InputFileSpec(file=Path("/path/to/infile"),
                                                    md5_file=Path("outfile.source_md5"),
                                                    formats=[Format.FASTQ, Format.GZIP]),
                                      OutputFileSpec(file=Path("outfile"),
                                                     md5_file=Path("outfile.md5"),
                                                     formats=[Format.UNMAPPED_CRAM]))) == \
        "set -o pipefail -o errexit -o errtrace; " + \
        "cat /path/to/infile | " + \
        'calcMd5 -f infile -o outfile.source_md5 | '\
        "gunzip -c /dev/stdin > /dev/stdout | " + \
        "(cat /dev/stdin > /dev/stdout) | " + \
        "fastqToSam.py --fastq-in /dev/stdin --comment-format illumina | " + \
        "samtools view --output-fmt cram,embed_ref=1 -o /dev/stdout | " + \
        'calcMd5 -f outfile -o outfile.md5 > outfile'

    assert conversion_command(MapSpec(InputFileSpec(file=Path("/path/to/infile"),
                                                    md5_ref=Path("outfile.md5sum"),
                                                    formats=[Format.FASTQ, Format.GZIP],
                                                    comment_format=CommentFormat.OPAQUE,
                                                    read_number_suffix=True),
                                      OutputFileSpec(file=Path("outfile"),
                                                     md5_file=Path("outfile.md5"),
                                                     formats=[Format.UNMAPPED_CRAM],
                                                     sam_headers=["@RG bla", "@RG blu"],
                                                     sam_attributes=["CO:Z:bla", "RG:Z:blu"],
                                                     read_number=1,
                                                     comment_format=CommentFormat.OPAQUE,
                                                     max_barcodes=10))) == \
           "set -o pipefail -o errexit -o errtrace; " + \
           'cat /path/to/infile | ' + \
           'calcMd5 -f infile -e outfile.md5sum | ' + \
           "gunzip -c /dev/stdin > /dev/stdout | " + \
           "(cat /dev/stdin > /dev/stdout) | " + \
           "fastqToSam.py --fastq-in /dev/stdin --max-barcodes 10 --comment-format opaque " + \
           "--header '@RG bla' --header '@RG blu' " + \
           "--attribute CO:Z:bla --attribute RG:Z:blu --read-number 1 | " + \
           "samtools view --output-fmt cram,embed_ref=1 -o /dev/stdout | " + \
           'calcMd5 -f outfile -o outfile.md5 > outfile'


def test_parse_args():
    r1 = parse_args(["-i", "infile",
                     "-I", "FASTQ,GZIP",
                     "-X", "inMd5RefFile",
                     "-5", "inMd5RefStr",
                     "-x", "inMd5Out",
                     "-o", "outfile",
                     "-O", "FASTQ,BZIP2",
                     "-y", "outMd5File",
                     "-H", "header1",
                     "-H", "header2",
                     "-a", "attrib1",
                     "-a", "attrib2",
                     "-n", "1",
                     "-r",
                     "-C", "100",
                     "-k", "opaque",
                     "-b", "1000"])
    assert r1.infile == Path("infile")
    assert r1.infile_format == [Format.FASTQ, Format.GZIP]
    assert r1.in_md5_ref_file == Path("inMd5RefFile")
    assert r1.in_md5_ref_string == "inMd5RefStr"
    assert r1.in_md5_file == Path("inMd5Out")
    assert r1.outfile == Path("outfile")
    assert r1.outfile_format == [Format.FASTQ, Format.BZIP2]
    assert r1.out_md5_file == Path("outMd5File")
    assert r1.sam_headers == ["header1", "header2"]
    assert r1.sam_attributes == ["attrib1", "attrib2"]
    assert r1.read_number == 1
    assert r1.read_number_suffix
    assert r1.read_count == 100
    assert r1.comment_format == CommentFormat.OPAQUE
    assert r1.max_barcodes == 1000
