# SPDX-FileCopyrightText: 2023 The OTP authors
#
# SPDX-License-Identifier: MIT

import sys
from io import StringIO
from pathlib import Path
from textwrap import dedent

import pytest
from fastqToSam import parse_fastq_entry, parse_fastq_header, main, parse_args

from lib import SeqEntry, OpaqueComment, IlluminaComment, CommentFormat


def test_parse_header():
    with pytest.raises(RuntimeError):
        parse_fastq_header("")
    assert parse_fastq_header("@identifier") == \
           ("identifier", None, None)
    assert parse_fastq_header("@identifier/1") == \
           ("identifier", 1, None)
    assert parse_fastq_header("@identifier/2") == \
           ("identifier", 2, None)
    with pytest.raises(RuntimeError):
        print(parse_fastq_header("@identifier/3"), file=sys.stderr)
    assert parse_fastq_header("@identifier/1 comment") == \
           ("identifier", 1, OpaqueComment.create("comment"))
    assert parse_fastq_header("@identifier/2 composite comment") == \
           ("identifier", 2, OpaqueComment.create("composite comment"))
    assert parse_fastq_header("@identifier/2 1:N:0:somebarcode", IlluminaComment.create) == \
           ("identifier", 2, IlluminaComment.create("1:N:0:somebarcode"))


def test_parse_fastq_entry():
    # A read with mate indicator is always interpreted as paired.
    assert SeqEntry(identifier="identifier",
                    paired=True,
                    read_number=1,
                    comment=OpaqueComment("comment"),
                    sequence="abc",
                    qualities="000") == \
           parse_fastq_entry(StringIO(dedent("""\
                                             @identifier/1 comment
                                             abc
                                             +
                                             000
                                             """)))
    assert SeqEntry(identifier="identifier",
                    sequence="abc",
                    paired=False,
                    qualities="000") == \
           parse_fastq_entry(StringIO(dedent("""\
                                             @identifier
                                             abc
                                             +
                                             000
                                             """)))

    # Qualities and sequence have different lengths.
    with pytest.raises(ValueError):
        parse_fastq_entry(StringIO(dedent("""\
                                          @identifier
                                          abc
                                          +
                                          000000000
                                          """)))


def test_main():
    fq1 = dedent("""\
                 @identifier comment
                 abc
                 +
                 000
                 """)
    fq2 = dedent("""\
                 @identifier/1 1:Y:1:ABC
                 abc
                 +
                 123
                 """)
    fq3 = dedent("""\
                 @identifier/2 2:N:0:BBB
                 cba
                 +
                 321
                 """)
    fq4 = dedent("""\
                 @identifier/2
                 xyz
                 +
                 111
                 """)

    # On empty input
    out1 = StringIO()
    main(StringIO(""),
         out1,
         headers=[],
         comment_format=CommentFormat.OPAQUE,
         attributes=[],
         read_number=None,
         expected_read_count=0,
         max_barcodes=None,
         command=["hello", "--header", "@PG value with	and @"])  # tabulator between with/and
    assert out1.getvalue() == \
           "@PG\tID:fastqToSam.py\tPN:fastqToSam.py\tCL:hello --header $'@PG value with\\tand @'\n"

    out2 = StringIO()
    main(StringIO(fq1),
         out2,
         headers=[],
         comment_format=CommentFormat.OPAQUE,
         attributes=[],
         expected_read_count=1,
         read_number=None,
         max_barcodes=None)
    assert out2.getvalue() == "".join([
        "@PG\tID:fastqToSam.py\tPN:fastqToSam.py\n",
        "\t".join([
            "identifier", "4", "*", "0", "0", "*", "*", "0", "0", "abc", "000", "XF:Z:comment"
        ]) + "\n"
    ])

    # Override to R1
    out2_1 = StringIO()
    main(StringIO(fq1),
         out2_1,
         headers=[],
         comment_format=CommentFormat.OPAQUE,
         attributes=[],
         max_barcodes=None,
         expected_read_count=None,
         read_number=1)
    assert out2_1.getvalue() == "".join([
        "@PG\tID:fastqToSam.py\tPN:fastqToSam.py\n",
        "\t".join([
            "identifier", "77", "*", "0", "0", "*", "*", "0", "0", "abc", "000", "XF:Z:comment"
        ]) + "\n"
    ])

    # Override to R2
    out2_2 = StringIO()
    main(StringIO(fq1),
         out2_2,
         headers=[],
         comment_format=CommentFormat.OPAQUE,
         attributes=[],
         max_barcodes=None,
         expected_read_count=1,
         read_number=2)
    assert out2_2.getvalue() == "".join([
        "@PG\tID:fastqToSam.py\tPN:fastqToSam.py\n",
        "\t".join([
            "identifier", "141", "*", "0", "0", "*", "*", "0", "0", "abc", "000", "XF:Z:comment"
        ]) + "\n"
    ])

    out3 = StringIO()
    main(StringIO(fq2),
         out3,
         headers=[],
         comment_format=CommentFormat.OPAQUE,
         attributes=[],
         read_number=None,
         expected_read_count=1,
         max_barcodes=None)
    assert out3.getvalue() == "".join([
        "@PG\tID:fastqToSam.py\tPN:fastqToSam.py\n",
        "\t".join([
            "identifier", "77", "*", "0", "0", "*", "*", "0", "0", "abc", "123", "XF:Z:1:Y:1:ABC"
        ]) + "\n"
    ])

    out4 = StringIO()
    main(StringIO(fq2 + fq3),
         out4,
         headers=[],
         comment_format=CommentFormat.ILLUMINA,
         attributes=[],
         expected_read_count=2,
         read_number=None,
         max_barcodes=None)
    assert out4.getvalue() == "".join([
        "@PG\tID:fastqToSam.py\tPN:fastqToSam.py\n",
        "\t".join([
            "identifier",  "589", "*", "0", "0", "*", "*", "0",
            "0", "abc", "123", "BC:Z:ABC", "XC:i:1"
        ]) + "\n",
        "\t".join([
            "identifier", "141", "*", "0", "0", "*", "*", "0",
            "0", "cba", "321", "BC:Z:BBB", "XC:i:0"
        ]) + "\n"
    ])

    out5 = StringIO()
    header = ":".join([
        "@RG\trgid",
        "barcode",
        "seqcenter",
        "description",
        "rundate",
        "floworder",
        "ks",
        "library",
        "progs",
        "medianinsertsize",
        "platform",
        "model",
        "unit",
        "sampleorpool"])
    main(StringIO(fq1),
         out5,
         headers=[header],
         comment_format=CommentFormat.OPAQUE,
         attributes=["CO:Z:static comment"],
         expected_read_count=1,
         read_number=None,
         max_barcodes=None)
    assert out5.getvalue() == "".join([
        "@PG\tID:fastqToSam.py\tPN:fastqToSam.py\n",
        header + "\n",
        "\t".join([
            "identifier", "4", "*", "0", "0", "*", "*", "0", "0", "abc", "000",
            "XF:Z:comment", "CO:Z:static comment"
        ]) + "\n"
    ])

    # Check whether there are too many barcodes.
    out6 = StringIO()
    with pytest.raises(RuntimeError):
        main(StringIO(fq2 + fq3),
             out6,
             headers=[],
             comment_format=CommentFormat.ILLUMINA,
             attributes=[],
             expected_read_count=None,
             read_number=None,
             max_barcodes=1)

    out7 = StringIO()
    main(StringIO(fq4),
         out7,
         headers=[header],
         comment_format=CommentFormat.OPAQUE,
         attributes=["CO:Z:static comment"],
         expected_read_count=1,
         read_number=None,
         max_barcodes=None)
    assert out7.getvalue() == "".join([
        "@PG\tID:fastqToSam.py\tPN:fastqToSam.py\n",
        header + "\n",
        "\t".join([
            "identifier", "141", "*", "0", "0", "*", "*", "0", "0", "xyz", "111",
            "CO:Z:static comment"
        ]) + "\n"
    ])

    with pytest.raises(ValueError):
        out8 = StringIO()
        main(StringIO(fq1 + fq2 + fq3),
             out8,
             headers=[header],
             comment_format=CommentFormat.OPAQUE,
             attributes=["CO:Z:static comment"],
             expected_read_count=2,   # should be 3
             read_number=None,
             max_barcodes=None)


def test_parse_args():
    r1 = parse_args(["-i", "infile",
                     "-o", "outfile",
                     "-H", "header1",
                     "-H", "header2",
                     "-a", "attrib1",
                     "-a", "attrib2",
                     "-n", "1",
                     "-C", "10",
                     "-b", "100",
                     "-k", "opaque"
                     ])
    assert r1.fastq == Path("infile")
    assert r1.sam == Path("outfile")
    assert r1.headers == ["header1", "header2"]
    assert r1.attributes == ["attrib1", "attrib2"]
    assert r1.read_number == 1
    assert r1.read_count == 10
    assert r1.max_barcodes == 100
    assert r1.comment_format == CommentFormat.OPAQUE

    r2 = parse_args(["-i", "infile",
                     "-o", "outfile"])
    assert r2.fastq == Path("infile")
    assert r2.sam == Path("outfile")
    assert r2.headers == []
    assert r2.attributes == []
    assert r2.read_number is None
    assert r2.read_count is None
    assert r2.max_barcodes is None
    assert r2.comment_format == CommentFormat.ILLUMINA

    r3 = parse_args(["--fastq-in", "infile",
                     "--sam-out", "outfile",
                     "--header", "header1",
                     "--header", "header2",
                     "--attribute", "attrib1",
                     "--attribute", "attrib2",
                     "--read-number", "1",
                     "--read-count", "10",
                     "--max-barcodes", "100",
                     "--comment-format", "opaque"
                     ])
    assert r3.fastq == Path("infile")
    assert r3.sam == Path("outfile")
    assert r3.headers == ["header1", "header2"]
    assert r3.attributes == ["attrib1", "attrib2"]
    assert r3.read_number == 1
    assert r3.read_count == 10
    assert r3.max_barcodes == 100
    assert r3.comment_format == CommentFormat.OPAQUE
