# SPDX-FileCopyrightText: 2023 The OTP authors
#
# SPDX-License-Identifier: MIT
from textwrap import dedent

import pytest

from lib import SeqEntry, OpaqueComment, IlluminaComment, CommentFormat


def test_from_string():
    assert CommentFormat.from_string("illumina") == CommentFormat.ILLUMINA
    assert CommentFormat.from_string("opaque") == CommentFormat.OPAQUE


def test_illumina_comment():
    t1 = IlluminaComment.create("1:N:2:ABC")
    assert t1.read_number == 1
    assert not t1.is_filtered
    assert t1.control_number == 2
    assert t1.barcode_sequence == "ABC"
    assert str(t1) == "1:N:2:ABC"

    t2 = IlluminaComment.create("2:Y:5:CBA")
    assert t2.read_number == 2
    assert t2.is_filtered
    assert t2.control_number == 5
    assert t2.barcode_sequence == "CBA"
    assert str(t2) == "2:Y:5:CBA"

    with pytest.raises(RuntimeError):
        _ = IlluminaComment.create("1:X:1:ABC").is_filtered

    with pytest.raises(RuntimeError):
        _ = IlluminaComment.create("X:Y:1:ABC").read_number

    with pytest.raises(RuntimeError):
        _ = IlluminaComment.create("1:N:X:ABC").control_number


def test_seqentry():
    seq_entry = SeqEntry(identifier="identifier",
                         paired=False,
                         read_number=None,
                         comment=IlluminaComment.create("1:N:0:ATGC"),
                         sequence="abc",
                         qualities="000")
    # Do not take the read number from the Illumina comment, if the read is not marked paired.
    # This is because the comment value is 1 also for unpaired reads.
    assert seq_entry.read_number is None
    assert not seq_entry.paired
    assert seq_entry.comment.read_number == 1

    seq_entry.read_number = 2
    assert seq_entry.read_number == 2
    # When setting the read number on the read, also mark it as paired automatically.
    assert seq_entry.paired
    # The read number in the comment is not updated by setting the value on the seq.
    assert seq_entry.comment.read_number == 1
    assert str(seq_entry.comment) == "1:N:0:ATGC"

    seq_entry2 = SeqEntry(identifier="identifier",
                          paired=False,
                          read_number=2,
                          comment=IlluminaComment.create("1:N:0:ATGC"),
                          sequence="abc",
                          qualities="000")
    # If a read number is set, the paired constructor values is overridden with the value True.
    assert seq_entry2.paired


def test_seqentry_fastq():
    assert SeqEntry(identifier="identifier",
                    paired=True,
                    read_number=1,
                    comment=OpaqueComment("comment"),
                    sequence="abc",
                    qualities="000").to_fastq(write_read_number_suffix=True) == dedent("""\
                        @identifier/1 comment
                        abc
                        +
                        000
                        """)
    assert SeqEntry(identifier="identifier",
                    sequence="abc",
                    qualities="000").to_fastq() == dedent("""\
                        @identifier
                        abc
                        +
                        000
                        """)


def test_seqentry_equality():
    values = {
        "identifier": "a",
        "sequence": "abc",
        "qualities": "...",
        "read_number": None,
        "comment": None
    }
    seq1 = SeqEntry(**values)
    seq2 = SeqEntry(**values)
    seq3 = SeqEntry(**{**values, "read_number": 1})
    assert seq1 == seq2
    assert seq1 != seq3
    assert seq2 != seq3
    assert seq3 == seq3


def test_seqentry_to_sam_line():
    values_opaque = {
        "identifier": "a",
        "sequence": "abc",
        "qualities": "...",
        "read_number": None,
        "comment": OpaqueComment.create("test")
    }
    seq_opaque = SeqEntry(**values_opaque)
    assert (seq_opaque.
            to_sam_line(comment_format=CommentFormat.OPAQUE,
                        static_tags=[]) == "\t".join([
                            "a", "4", "*", "0", "0", "*", "*", "0", "0", "abc", "...",
                            "XF:Z:test"
                        ]) + "\n")

    assert (seq_opaque.
            to_sam_line(comment_format=CommentFormat.OPAQUE,
                        static_tags=["RG:1:hello"],
                        opaque_comment_tag="X1",
                        barcode_tag="X2",
                        control_bits_tag="X3") == "\t".join([
                            "a", "4", "*", "0", "0", "*", "*", "0", "0", "abc", "...",
                            "X1:Z:test", "RG:1:hello"
                        ]) + "\n")

    values_illum = {
        "identifier": "a",
        "sequence": "abc",
        "qualities": "...",
        "read_number": None,
        "comment": IlluminaComment.create("1:Y:1:barcode")
    }
    seq_illum = SeqEntry(**values_illum)
    assert (seq_illum.
            to_sam_line(comment_format=CommentFormat.OPAQUE,
                        static_tags=[]) == "\t".join([
                            "a", "516", "*", "0", "0", "*", "*", "0", "0", "abc", "...",
                            "XF:Z:1:Y:1:barcode"
                        ]) + "\n")

    assert (seq_illum.
            to_sam_line(comment_format=CommentFormat.ILLUMINA,
                        opaque_comment_tag="X1",
                        barcode_tag="X2",
                        control_bits_tag="X3") == "\t".join([
                            "a", "516", "*", "0", "0", "*", "*", "0", "0", "abc", "...",
                            "X2:Z:barcode", "X3:i:1"
                        ]) + "\n")
