<!--
SPDX-FileCopyrightText: 2023 The OTP authors

SPDX-License-Identifier: MIT
-->

# Integration Tests

## Requirements & Setup

The integration tests need the Nextflow and samtools for the actual test code (run Nextflow and check the results with samtools), and dependent on your choice of profile, "mamba", "conda", or "docker". Generally, we suggest that you run the tests with the Nextflow and Samtools from the Conda environment in `integration-tests/environment.yaml`. 

Specifically,

1. Get an installation of Mamba (e.g. Mambaforge3)
2. Optionally get Docker running, if you want to run the "docker" profile
3. Build the Groovy library that is part of the workflow and install it in the `lib/` directory
   ```bash
   cd $workflowRepositoryDir
   ./gradlew build
   ```
4. Install Nextflow and Samtools for the tests
   ```bash
   cd integration-tests
   conda env create -n nf-seq-convert -f environment.yml
   ```

> NOTE: The environment in `envs/nf-seq-convert.yml` is the environment active in the jobs that the workflow runs. By contrast, the environment in `integration-tests/environment.yaml` is an environment with Nextflow, Samtools, and some other tools that you can use for the integration tests and the development.  

## Running the tests

To run the tests do

```bash
cd integration-tests
run.sh (mamba|docker|apptainer) outDir/
```

using one of the three possible profiles.

Input and comparison data for the tests can be found in the `resources/` directory. The output data go into the `outDir/`.
