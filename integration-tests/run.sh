#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2023 The OTP authors
#
# SPDX-License-Identifier: MIT

set -ue
set -o pipefail

trap 'echo "Test execution failed" >> /dev/stderr; exit $?' ERR

testsDir="$(readlink -f "$(dirname "${BASH_SOURCE[0]}")")"

environmentProfile="${1:-mamba}"
outputDir="$(readlink -f "${2:?No outputDir given}")"  # Nextflow needs absolute path.
nextflowEnvironment="${3:-$testsDir/nextflowEnv}"

# Use mamba if possible.
if command -v mamba; then
  condaBinary=mamba
else
  condaBinary=micromamba
fi

workflowDir="$testsDir/.."
mkdir -p "$outputDir"

# Setup the test environment (nextflow).
if [[ ! -d "$nextflowEnvironment" ]]; then
  $condaBinary env create -v -f "$workflowDir/integration-tests/environment.yml" -p "$nextflowEnvironment"
fi
set +ue
eval "$($CONDA_EXE shell.bash hook)"
conda activate "$nextflowEnvironment"
set -ue

TEST_TOTAL=0
TEST_ERRORS=0


assertTrue() {
  local expr="${1:?No expression given}"
  local message="${2:?No message given}"
  TEST_TOTAL=$((TEST_TOTAL + 1))
  if $expr; then
    echo "Success: $message" >> /dev/stderr
  else
    echo "Failure: $message" >> /dev/stderr
    TEST_ERRORS=$((TEST_ERRORS + 1))
  fi
}

assertEqual() {
  local observed="${1:?No observed number given}"
  local expected="${2:?No expected number given}"
  local message="${3:?No message given}"
  TEST_TOTAL=$((TEST_TOTAL + 1))
  if [[ "$observed" == "$expected" ]]; then
    echo "Success: $message" >> /dev/stderr
  else
    echo "Failure: $message: observed $observed != expected $expected" >> /dev/stderr
    TEST_ERRORS=$((TEST_ERRORS + 1))
  fi
}

testFinished() {
  echo "" >> /dev/stderr
  echo "$TEST_ERRORS of $TEST_TOTAL tests failed." >> /dev/stderr
  if [[ $TEST_ERRORS -gt 0 ]]; then
    exit 1
  else
    exit 0
  fi
}

# Keep Nextflow memory footprint small (needed for CI)
export NXF_OPTS="-Xmx100m"

outputDir1="$outputDir/test1"
mkdir -p "$outputDir1"

# Run the tests.
# Please add valid testing parameters here!
test1() {
  nextflow run "$workflowDir/main.nf" \
    -profile "test,$environmentProfile" \
    -params-file test1.yaml \
    --outputDir "$outputDir1"
}
code=0
test1 || code=$?
assertTrue "test $code -eq 0" "Nextflow test1 run"

# Same number of files and directories
assertEqual "$(find "$outputDir1" -type f -or -type l | wc -l)" 15 "Number of output files and links in $outputDir1"

# File names refer to symlinks (rellink)
for f in run1_gerald_D1VCPACXX_1_R1.sorted.ucram \
         run1_gerald_D1VCPACXX_1_R1.sorted.ucram.md5 \
         run1_gerald_D1VCPACXX_1_R1.sorted.ucram.source_md5 \
         subdir/run1_gerald_D1VCPACXX_1_R2.sam.bz2 \
         subdir/run1_gerald_D1VCPACXX_1_R2.sam.bz2.md5 \
         subdir/run1_gerald_D1VCPACXX_1_R2.sam.bz2.source_md5;
         do
  assertTrue "test -h $outputDir1/$f" "Symlink '$f'"
done

test1Results="resources/test1-results"
# R1
assertEqual "$(samtools view -h "$outputDir1/run1_gerald_D1VCPACXX_1_R1.sorted.ucram" | grep -v "CL:samtools" | md5sum | cut -f 1 -d ' ')" \
            "$(samtools view -h "$test1Results/run1_gerald_D1VCPACXX_1_R1.sorted.ucram" | grep -v "CL:samtools" | md5sum | cut -f 1 -d ' ')" \" \
            "Content of R1 output (CRAM)"

assertEqual "$(md5sum "$outputDir1/run1_gerald_D1VCPACXX_1_R1.sorted.ucram" | cut -f 1 -d ' ')" \
            "$(cat "$outputDir1/run1_gerald_D1VCPACXX_1_R1.sorted.ucram.md5" | cut -f 1 -d' ')" \
            "MD5 file of CRAM file (R1 output)"

assertEqual "$(cat "$outputDir1/run1_gerald_D1VCPACXX_1_R1.sorted.ucram.source_md5" | cut -f 1 -d ' ')" \
            "bfbe3757192ae855ff2c3f2a5b8d4619" \
            "MD5 of R1 FASTQ"

assertEqual "$(zcat "$testsDir/resources/run1_gerald_D1VCPACXX_1_R1.sorted.fastq.gz"  | paste - - - - | sed 's/^@//' | cut -f 1,2,4 | md5sum)" \
            "$(samtools view "$outputDir1/run1_gerald_D1VCPACXX_1_R1.sorted.ucram" | cut -f 1,10,11 | md5sum)" \
            "Same reads in same order (R1)"

# R2
assertEqual "$(bunzip2 -c "$outputDir1/subdir/run1_gerald_D1VCPACXX_1_R2.sam.bz2" | md5sum | cut -f 1 -d ' ')" \
            "$(bunzip2 -c "$test1Results/run1_gerald_D1VCPACXX_1_R2.sam.bz2" | md5sum | cut -f 1 -d ' ')" \
            "Content of R2 output (.sam.bz2)"

assertEqual "$(md5sum "$outputDir1/subdir/run1_gerald_D1VCPACXX_1_R2.sam.bz2" | cut -f 1 -d ' ')" \
            "$(cat "$outputDir1/subdir/run1_gerald_D1VCPACXX_1_R2.sam.bz2.md5" | cut -f 1 -d' ')" \
            "MD5 file of SAM-BZip2 file (R2 output)"

assertEqual "$(cat "$outputDir1/subdir/run1_gerald_D1VCPACXX_1_R2.sam.bz2.source_md5" | cut -f 1 -d ' ')" \
            "77e00e615abdc0985d118aee16515f44" \
            "MD5 of R2 FASTQ"

assertEqual "$(zcat "$testsDir/resources/run1_gerald_D1VCPACXX_1_R2.sorted.fastq.gz"  | paste - - - - | sed 's/^@//' | cut -f 1,2,4 | md5sum)" \
            "$(bunzip2 -c "$outputDir1/subdir/run1_gerald_D1VCPACXX_1_R2.sam.bz2" | samtools view - | cut -f 1,10,11 | md5sum)" \
            "Same reads in same order (R2)"

# empty input
assertEqual "$(gunzip -c "$outputDir1/empty.sam.gz" | md5sum | cut -f 1 -d ' ')" \
            "$(gunzip -c "$test1Results/empty.sam.gz" | md5sum | cut -f 1 -d ' ')" \
            "Content of empty output (.sam.gz)"

assertEqual "$(md5sum "$outputDir1/empty.sam.gz" | cut -f 1 -d ' ')" \
            "$(cat "$outputDir1/empty.sam.gz.md5" | cut -f 1 -d' ')" \
            "MD5 file of SAM-Gzip file (empty output)"

assertEqual "$(cat "$outputDir1/empty.sam.gz.source_md5" | cut -f 1 -d ' ')" \
            "e1996cc799ac2f5ffc62c8996a37acdd" \
            "MD5 of empty FASTQ"

# barcodes, keep comments, more file suffixes
assertEqual "$(gunzip -c "$outputDir1/1barcode.sam.gz" | samtools view - | md5sum | cut -f 1 -d ' ')" \
            "$(gunzip -c "$test1Results/1barcode.sam.gz" | samtools view - | md5sum | cut -f 1 -d ' ')" \
            "Contents when keeping FASTQ comments opaquely"

assertEqual "$(cat "$outputDir1/2barcodes.sam" | samtools view - | md5sum | cut -f 1 -d ' ')" \
            "22944f21da2184a3540f46faac2ab224" \
            "Contents when interpreting FASTQ comments as Illumina comments"

# MD5 of source files had a special name
assertEqual "$(cat "$outputDir1/source_file.md5" | cut -f 1 -d ' ')" \
            "7b8c71d4bab6e941850744f9700b86e8" \
            "MD5 of 2barcodes.fastq.bz2 source file"

## Failing Nextflow run
outputDir2="$outputDir/test2"
mkdir -p "$outputDir2"

echo "A failing nextflow run" >> /dev/stderr
test2ErrFile="$outputDir2/test2.err"
test2() {
  nextflow run "$workflowDir/main.nf" \
    -profile "test,$environmentProfile" \
    -params-file test2.yaml \
    --outputDir "$outputDir2" \
    | grep -A 1 "Work dir:" \
    | tail -n 1 \
    | sed 's/^\s*//' \
    > "$test2ErrFile"
}
code=0
test2 || code=$?
assertTrue "test $code -ne 0" "Nextflow test2 run (should fail)"

barcodeTestErrFile="$(cat "$test2ErrFile")/.command.err"
assertEqual "$(grep "RuntimeError:" "$barcodeTestErrFile")" \
            "RuntimeError: Number of observed barcodes 2 is larger than requested maximum 1! {'TTTTTTTT': 1, 'AAAAAAAA': 1}" \
            "Observed barcodes error"

## Incorrect infile MD5 sum
outputDir3="$outputDir/test3"
mkdir -p "$outputDir3"

echo "A failing nextflow run" >> /dev/stderr
test3ErrFile="$outputDir/test3.err"
test3() {
  nextflow run "$workflowDir/main.nf" \
    -profile "test,$environmentProfile" \
    -params-file test3.yaml \
    --outputDir "$outputDir3" \
    | grep -A 1 "Work dir:" \
    | tail -n 1 \
    | sed 's/^\s*//' \
    > "$test3ErrFile"
}
code=0
test3 || code=$?
assertTrue "test $code -ne 0" "Nextflow test3 run (should fail)"

# Reverse test SAM->FASTQ
outputDir4="$outputDir/test4"
mkdir -p "$outputDir4"
test5() {
nextflow run "$workflowDir/main.nf" \
  -profile "test,$environmentProfile" \
  -params-file test4.yaml \
  --outputDir "$outputDir4"
}
code=0
test5 || code=$?
assertTrue "test $code -eq 0" "Nextflow test4 run"

# The workflow changes the number of spaces between read ID and comment. Therefore ignore whitespace changes.
diff -b -q <(bunzip2 -c "$testsDir/resources/1barcode.fastq.bz2") <(gunzip -c "$outputDir4/1barcode.fastq.gz")
assertTrue "test $? -eq 0" "Contents when keeping FASTQ comments opaquely"

testFinished
