#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 The OTP authors
#
# SPDX-License-Identifier: MIT
import argparse
import re
import sys
from collections import defaultdict
from pathlib import Path
from typing import TextIO, List, Dict, Set, Optional, Generator, Tuple, TypeVar, Union

from lib import (
    OpaqueComment, SeqEntry, CommentFormat, IlluminaComment,
    SamFlag, AbstractComment, limit_heap_size)


def parse_tags(tags: List[str]) -> Dict[str, Set[str]]:
    """
    Get a dictionary of tag keys to tag value sets. Some keys may occur multiple times,
    which is why a multidict is returned. Note that sets are returned. Thus, if the same
    value is present multiple times, only a single representative is returned.
    """
    result = defaultdict(set)
    for tag_string in tags:
        tag_name, _, *tag_value = tag_string.split(":")   # ignore type code
        result[tag_name].add(":".join(tag_value))
    return result


def extract_flag_information(flags: int) \
        -> Tuple[Optional[int], int, bool]:
    """
    Extract information from the SAM flags. These are returned as tuple

      * read number
      * read number for comments (depends on SAM paired flag)
      * quality filtering
    """
    if not (flags & SamFlag.PAIRED):
        # Casava 1.8 user manual, page 80. Read number should be 1 for single-end reads.
        # Note that without the PAIRED flag, the read will be 1, even if SECOND_IN_PAIR is set.
        read_number = None
        comment_read_number = 1
    elif (flags & SamFlag.FIRST_IN_PAIR) and (flags & SamFlag.SECOND_IN_PAIR):
        raise RuntimeError("Read marked both as first and second read of a pair!")
    elif flags & SamFlag.FIRST_IN_PAIR:
        read_number = 1
        comment_read_number = 1
    elif flags & SamFlag.SECOND_IN_PAIR:
        read_number = 2
        comment_read_number = 2
    else:
        raise RuntimeError("Cannot happen.")

    is_filtered = bool(flags & SamFlag.IS_FILTERED)

    return read_number, comment_read_number, is_filtered


def create_comment(comment_format: CommentFormat,
                   comment_read_number: int,
                   is_filtered: bool,
                   opaque_comment: Optional[str],
                   barcode: Optional[str],
                   control_bits: Optional[Union[str, int]]) \
        -> Optional[AbstractComment]:
    """
    Try to create a comment object from the provided information.
    """
    comment: Optional[AbstractComment]
    if comment_format == CommentFormat.ILLUMINA:
        if barcode is None:
            barcode = ""
        if control_bits is None:
            control_bits = 0
        comment = IlluminaComment.create("%s:%s:%s:%s" % (
            comment_read_number,
            "Y" if is_filtered else "N",
            int(control_bits),
            barcode))

    elif comment_format == CommentFormat.OPAQUE:
        if opaque_comment is not None:
            comment = OpaqueComment.create(opaque_comment)
        else:
            comment = None

    else:
        raise RuntimeError("Oops! Bug.")

    return comment


T = TypeVar("T")


def assert_unique_tag(seq_identifier, tags_name, tag_values: Optional[Set[str]]) -> Optional[str]:
    if tag_values is None:
        return None
    elif len(tag_values) > 1:
        raise RuntimeError(f"Multiple {tags_name} values for read {seq_identifier}: " +
                           str(tag_values))
    else:
        return list(tag_values)[0]


def parse_sam_line(line: str,
                   comment_format: CommentFormat,
                   opaque_comment_tag: str = "XF",
                   barcode_tag: str = "BC",
                   control_bits_tag: str = "XC"
                   ) \
        -> SeqEntry[AbstractComment]:
    qname, flag_str, rname, pos, mapq, cigar, rnext, pnext, tlen, seq, qual, *tag_strings = (
        line.rstrip().split("\t"))

    read_number, comment_read_number, is_filtered = extract_flag_information(int(flag_str))

    tags = parse_tags(tag_strings)

    comment = create_comment(comment_format,
                             comment_read_number,
                             is_filtered,
                             opaque_comment=assert_unique_tag(qname, opaque_comment_tag,
                                                              tags.get(opaque_comment_tag)),
                             barcode=assert_unique_tag(qname, barcode_tag,
                                                       tags.get(barcode_tag)),
                             control_bits=assert_unique_tag(qname, control_bits_tag,
                                                            tags.get(control_bits_tag)))

    if len(seq) != len(qual):
        raise ValueError(f"Possibly corrupt read {qname}/{read_number}:\n" +
                         "\t" + seq + "\n" +
                         "\t" + qual + "\n")

    return SeqEntry[AbstractComment](identifier=qname,
                                     sequence=seq,
                                     qualities=qual,
                                     read_number=read_number,
                                     comment=comment)


SAM_HEADER_PATTERN = r"^@[A-Z][A-Z](?:\t[A-Za-z][A-Za-z0-9]:[ -~]+)+$|^@CO\t.*$"


def is_sam_header_line(line: str) -> bool:
    """
    Header lines match the given regular expressions. See SAMv1.pdf specification.
    """
    return bool(re.match(SAM_HEADER_PATTERN, line))


def parse_sam(handle: TextIO,
              comment_format: CommentFormat) \
        -> Generator[SeqEntry[AbstractComment], None, None]:
    for line in handle:
        if not is_sam_header_line(line) and line != "":
            yield parse_sam_line(line, comment_format)


def main(sam_fd: TextIO,
         fastq_fd: TextIO,
         comment_format: CommentFormat,
         read_number_suffix: bool,
         expected_read_count: Optional[int]) -> None:
    processed_reads = 0
    for seq_entry in parse_sam(sam_fd, comment_format):
        fastq_fd.write(seq_entry.to_fastq(read_number_suffix))
        processed_reads += 1
    if expected_read_count is not None and expected_read_count != processed_reads:
        raise ValueError(f"Processed {processed_reads} instead of expected "
                         f"{expected_read_count} reads.")
    print(f"samToFastq.py processed {processed_reads} reads.", file=sys.stderr)


def parse_args(args: List[str]):
    parser = argparse.ArgumentParser(
        description="Read in a SAM file (ASCII, a.k.a. TAM) and return a FASTQ formatted file."
    )
    parser.add_argument("-i", "--sam-in",
                        default=Path("/dev/stdin"),
                        dest="sam",
                        action="store",
                        type=Path,
                        help="Input SAM file. Defaults to standard input.")
    parser.add_argument("-o", "--fastq-out",
                        default=Path("/dev/stdout"),
                        dest="fastq",
                        action="store",
                        type=Path,
                        help="Output FASTQ file. Defaults to standard output.")
    parser.add_argument("-k", "--comment-format",
                        type=CommentFormat.from_string,
                        action="store",
                        dest="comment_format",
                        default=CommentFormat.ILLUMINA,
                        help="If 'opaque', the XF:Z tags in the SAM input is stripped of its "
                        "XF:Z: prefix and written as comment into the FASTQ. "
                        "If 'illumina', then Illumina-formatted comments are written "
                        "(Casava 1.8), i.e. $readNumber:$filtered:$controlNumber:"
                        "$barcode. The information will be retrieved from the SAM "
                        "file flags (filtered, readNumber) and tags (BC:Z for "
                        "barcode, and custom tag XC:i for controlNumber). If the XC tag is "
                        "missing, a control bits value of 0 is assumed. If the BC tag is "
                        "missing, a filtered values of 'N' is assumed and written to the "
                        "FASTQ. In both cases, a warning is issued to notify the user about "
                        "missing values and the interpolation of these defaults.")
    parser.add_argument("-s", "--read-number-suffix",
                        action="store_true",
                        dest="read_number_suffix",
                        help="If set, /1 and /2 will be attached as suffixes to the identifier "
                        "if the read is paired end first or second in pair, respectively. Unpaired "
                        "reads will not get any suffix")
    parser.add_argument("-C", "--read-count",
                        action="store",
                        type=int,
                        dest="read_count",
                        default=None,
                        help="Optionally, check that the input has the given number of reads.")
    return parser.parse_args(args)


if __name__ == "__main__":
    limit_heap_size(20, 30)
    args = parse_args(sys.argv[1:])
    with open(args.sam, "rt") as sam_fd:
        with open(args.fastq, "wt") as fastq_fd:
            main(sam_fd=sam_fd,
                 fastq_fd=fastq_fd,
                 comment_format=args.comment_format,
                 read_number_suffix=args.read_number_suffix,
                 expected_read_count=args.read_count)
