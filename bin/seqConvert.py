#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 The OTP authors
#
# SPDX-License-Identifier: MIT

from __future__ import annotations

import argparse
import re
import subprocess
import sys
from dataclasses import dataclass, field
from enum import Enum
from pathlib import Path
from shlex import quote as q
from typing import List, Optional, Callable, Tuple, Union

from lib import limit_heap_size, CommentFormat


def dropped(d, *args):
    return {k: v for k, v in d.items() if k not in args}


DEFAULT_COMMENT_FORMAT = CommentFormat.ILLUMINA


@dataclass
class FormatDetails:
    suffixes: List[str]

    # The unpack command transforms the input into the intermediate representation.
    unpack_command_template: Callable[..., str]
    # The pack command transform the intermediate representation into the output format.
    pack_command_template: Callable[..., str]
    # The intermediate representation is uncompressed FASTQ.

    def canonical_suffix(self) -> str:
        return self.suffixes[0]

    def suffix_matches(self, suffix: str) -> bool:
        return suffix.lstrip(".") in self.suffixes

    def unpack_command(self,
                       infile: Path = Path("/dev/stdin"),
                       outfile: Path = Path("/dev/stdout"),
                       **kwargs)\
            -> str:
        return self.unpack_command_template(infile=infile, outfile=outfile, **kwargs)

    def pack_command(self,
                     infile: Path = Path("/dev/stdin"),
                     outfile: Path = Path("/dev/stdout"),
                     **kwargs)\
            -> str:
        return self.pack_command_template(infile=infile, outfile=outfile, **kwargs)


class Format(Enum):
    FASTQ = FormatDetails(["fastq", "fq", "txt"],
                          lambda **k: f"(cat {q(str(k['infile']))} > {q(str(k['outfile']))})",
                          lambda **k: f"(cat {q(str(k['infile']))} > {q(str(k['outfile']))})")
    UNMAPPED_BAM = \
        FormatDetails(["bam", "ubam"],
                      lambda **k: f"samtools view -h {q(str(k['infile']))} | " +
                                  Format.sam_to_fastq_command(**k),
                      lambda **k: Format.fastq_to_sam_command(**dropped(k, "outfile")) +
                      f" | samtools view -b -o {q(str(k['outfile']))}")
    UNMAPPED_TAM = \
        FormatDetails(["sam", "usam"],
                      lambda **k: Format.sam_to_fastq_command(**k),
                      lambda **k: Format.fastq_to_sam_command(**k))
    UNMAPPED_CRAM = \
        FormatDetails(["cram", "ucram"],
                      lambda **k: f"samtools view -h {q(str(k['infile']))} | " +
                                  Format.sam_to_fastq_command(**k),
                      lambda **k: Format.fastq_to_sam_command(**dropped(k, "outfile")) +
                      f" | samtools view --output-fmt cram,embed_ref=1 -o {q(str(k['outfile']))}")
    GZIP = FormatDetails(["gz", "gzip"],
                         lambda **k: f"gunzip -c {q(str(k['infile']))} > {q(str(k['outfile']))}",
                         lambda **k: f"gzip -c {q(str(k['infile']))} > {q(str(k['outfile']))}")
    BZIP2 = FormatDetails(["bz2", "bzip2"],
                          lambda **k: f"bunzip2 -c {q(str(k['infile']))} > {q(str(k['outfile']))}",
                          lambda **k: f"bzip2 -c {q(str(k['infile']))} > {q(str(k['outfile']))}")

    @staticmethod
    def fastq_to_sam_command(infile: Optional[Path] = None,   # noqa: C901  Has simple structure.
                             outfile: Optional[Path] = None,
                             max_barcodes: Optional[int] = None,
                             comment_format: CommentFormat = DEFAULT_COMMENT_FORMAT,
                             sam_headers: Optional[List[str]] = None,
                             sam_attributes: Optional[List[str]] = None,
                             read_number: Optional[int] = None,
                             read_count: Optional[int] = None,
                             **kwargs) -> str:
        command = ["fastqToSam.py"]
        if infile is not None:
            command += ["--fastq-in", q(str(infile))]
        if outfile is not None:
            command += ["--sam-out", q(str(outfile))]
        if max_barcodes is not None:
            command += ["--max-barcodes", str(max_barcodes)]
        if comment_format:
            command += ["--comment-format", q(comment_format.name.lower())]
        if sam_headers is not None:
            for header in sam_headers:
                command += ["--header", q(header)]
        if sam_attributes is not None:
            for attribute in sam_attributes:
                command += ["--attribute", q(attribute)]
        if read_number is not None:
            command += ["--read-number", str(read_number)]
        if read_count is not None:
            command += ["--read-count", str(read_count)]
        return " ".join(command)

    @staticmethod
    def sam_to_fastq_command(infile: Optional[Path] = None,
                             outfile: Optional[Path] = None,
                             read_number_suffix: bool = False,
                             read_count: Optional[int] = None,
                             comment_format: CommentFormat = DEFAULT_COMMENT_FORMAT,
                             **kwargs):
        command = ["samToFastq.py"]
        if infile is not None:
            command += ["--sam-in", q(str(infile))]
        if outfile is not None:
            command += ["--fastq-out", q(str(outfile))]
        command += ["--comment-format", q(comment_format.name.lower())]
        if read_number_suffix:
            command += ["--read-number-suffix"]
        if read_count is not None:
            command += ["--read-count", str(read_count)]
        return " ".join(command)

    def __init__(self, format_details: FormatDetails):
        self._format_details = format_details

    def unpack_command(self, **kwargs) -> str:
        return self._format_details.unpack_command(**kwargs)

    def pack_command(self, **kwargs) -> str:
        return self._format_details.pack_command(**kwargs)

    def canonical_suffix(self) -> str:
        return self._format_details.canonical_suffix()

    @staticmethod
    def from_spec_name(format_spec_name: str) -> Format:
        try:
            return Format[format_spec_name.upper()]
        except KeyError:
            raise ValueError(f"Could not interpret format specification: '{format_spec_name}'")

    @staticmethod
    def _assert_valid_format(formats: List[Format]):
        """
        Throw ValueError, if the first Format of the list is not a sequence format.
        """
        if not formats[0].is_sequence_format():
            raise ValueError("The terminal format is not a sequence format: " +
                             f"'{formats[0]}'")

    @staticmethod
    def from_suffix(suffix: str) -> List[Format]:
        for format in Format:
            if format._format_details.suffix_matches(suffix):
                return [format]
        # If none matched then the suffix cannot be recognized.
        raise ValueError(f"Do not know suffix '{suffix}'")

    @staticmethod
    def from_suffixes(filename: Path, validate: bool = True) -> Tuple[Path, Optional[List[Format]]]:
        """
        Formats derived from the suffixes. Note that the assumption is that the formats are
        unambiguously derivable and the suffixes are ordered from innermost to outermost.

        By default, the innermost (first list element) format is checked to be a sequence format.
        """
        suffixes = filename.suffixes
        if suffixes is None:
            return filename, None
        else:
            formats: List[Format] = []
            idx: int = -1
            for idx in reversed(range(0, len(suffixes))):
                try:
                    formats += Format.from_suffix(suffixes[idx])
                except ValueError:
                    # Stop condition is the first string that does not correspond to a known suffix.
                    # This is to handle, e.g., filenames containing identifiers with dots, that
                    # may be parsed into "suffixes".
                    idx += 1
                    break
            formats.reverse()
            if len(formats) != 0 and validate:
                Format._assert_valid_format(formats)

            # The radix is the shortest stem, with all suffixes removed.
            radix = filename.name.removesuffix("".join(suffixes))
            # We may have matched only some suffixes, though, so re-add those that remained
            # unmatched to create the true stem.
            stem = "".join([radix] + suffixes[0:idx])

            return filename.parent / stem, formats

    @staticmethod
    def from_spec_names(format_spec_names: List[str], validate: bool = True) \
            -> Optional[List[Format]]:
        if len(format_spec_names) == 0:
            return None
        else:
            formats = [Format.from_spec_name(spec) for spec in format_spec_names]
            if validate:
                Format._assert_valid_format(formats)
            return formats

    def is_sequence_format(self) -> bool:
        return self in [Format.FASTQ,
                        Format.UNMAPPED_TAM,
                        Format.UNMAPPED_CRAM,
                        Format.UNMAPPED_BAM]


def parse_format_spec(format_spec: str) -> Optional[List[Format]]:
    return Format.from_spec_names(format_spec.split(","))


@dataclass
class InputFileSpec:
    file: Path
    formats: Optional[List[Format]] = None

    md5_ref: Optional[Union[str, Path]] = None
    md5_file: Optional[Path] = None

    read_count: Optional[int] = None
    read_number_suffix: bool = False

    comment_format: CommentFormat = DEFAULT_COMMENT_FORMAT


@dataclass
class OutputFileSpec:
    file: Path
    formats: Optional[List[Format]] = None

    md5_file: Optional[Path] = None
    max_barcodes: Optional[int] = None

    sam_attributes: List[str] = field(default_factory=lambda: [])
    sam_headers: List[str] = field(default_factory=lambda: [])

    read_count: Optional[int] = None
    read_number: Optional[int] = None

    comment_format: CommentFormat = DEFAULT_COMMENT_FORMAT


@dataclass
class MapSpec:
    input: InputFileSpec
    output: OutputFileSpec


def ensure_formats(map_spec: MapSpec):
    if map_spec.input.formats is None:
        _, map_spec.input.formats = \
            Format.from_suffixes(map_spec.input.file)

    if map_spec.output.formats is None:
        _, map_spec.output.formats = \
            Format.from_suffixes(map_spec.output.file)

    if map_spec.input.formats is None or len(map_spec.input.formats) == 0:
        raise RuntimeError("No input file format specified or derivable")
    if map_spec.output.formats is None or len(map_spec.output.formats) == 0:
        raise RuntimeError("No output file format specified or derivable")


def ensure_md5_files(map_spec: MapSpec):
    if map_spec.input.md5_file is None:
        if map_spec.output.file is not None:
            map_spec.input.md5_file = \
                map_spec.output.file.parent / (map_spec.output.file.name + ".source_md5")
        else:
            raise RuntimeError("Cannot derive input MD5 file name")

    if map_spec.output.md5_file is None:
        if map_spec.output.file is not None:
            map_spec.output.md5_file = \
                map_spec.output.file.parent / (map_spec.output.file.name + ".md5")
        else:
            raise RuntimeError("Cannot derive output MD5 file name")


def ensure_files(map_spec: MapSpec):
    if map_spec.input.file is None:
        map_spec.input.file = Path("/dev/stdin")
    if map_spec.output.file is None:
        map_spec.output.file = Path("/dev/stdout")


def ensure_md5_ref(inspec: InputFileSpec):
    if inspec.md5_ref is None:
        pass
    elif isinstance(inspec.md5_ref, Path):
        # Ensure that a path consisting only of characters valid in MD5 sums gets ./ as prefix.
        # This is done to force calcMd5 to interpret this as path, not as MD5.
        if re.match(r"^[0-9a-fA-F]{32}$", str(inspec.md5_ref)):
            inspec.md5_ref = Path("./") / inspec.md5_ref
    else:
        if not re.match(r"^[0-9a-fA-F]{32}$", str(inspec.md5_ref)):
            raise ValueError(f"Not an MD5 sum: '{inspec.md5_ref}'")


def to_map_spec(args) -> MapSpec:
    """
    Take the raw MapSpecs and fill in missing information. This is the logic
    described in the usage info on how the target file formats and names are determined.
    """
    in_md5_ref: Optional[Union[Path, str]]
    if args.in_md5_ref_string is not None and args.in_md5_ref_file is not None:
        raise ValueError(
            f"Both input --in-md5-ref ({args.in_md5_ref_string}) and "
            f"--in-md5-ref-file ({args.in_md5_ref_file}) are set.")
    elif args.in_md5_ref_string is not None:
        in_md5_ref = args.in_md5_ref_string
    else:
        in_md5_ref = args.in_md5_ref_file

    map_spec = MapSpec(InputFileSpec(args.infile,
                                     args.infile_format,
                                     in_md5_ref,
                                     args.in_md5_file,
                                     args.read_count,
                                     args.read_number_suffix,
                                     args.comment_format),
                       OutputFileSpec(args.outfile,
                                      args.outfile_format,
                                      args.out_md5_file,
                                      args.max_barcodes,
                                      args.sam_attributes,
                                      args.sam_headers,
                                      args.read_count,
                                      args.read_number,
                                      args.comment_format))

    ensure_formats(map_spec)
    ensure_md5_ref(map_spec.input)
    ensure_md5_files(map_spec)
    ensure_files(map_spec)

    return map_spec


def md5_command(streamed_file: Path,
                out_md5: Optional[Path] = None,
                in_md5_expected: Optional[Union[Path, str]] = None) \
        -> str:
    """
    A Bash command to calculate the MD5 on a stream of data, write the MD5 into a file, and
    compare it to an expected MD5.
    """
    command = ["calcMd5", "-f", str(streamed_file)]
    if out_md5 is not None:
        command += ["-o", str(out_md5)]
    if in_md5_expected is not None:
        if isinstance(in_md5_expected, Path):
            command += ["-e", str(in_md5_expected)]
        else:
            command += ["-E", str(in_md5_expected)]
    return " ".join([q(segment) for segment in command])


def unpack_command(infile_spec: InputFileSpec) -> List[str]:
    """
    Output of the command is supposed to go to standard output.

    The outfile_path is used only to create an MD5 file of the input. The MD5 file will have
    the same name (incl. suffixes) as the input file, but with .md5 as suffix.
    """
    if infile_spec.formats is None:
        raise RuntimeError("Oops")
    else:
        command = [
            f"cat {q(str(infile_spec.file))}"
        ]
        if infile_spec.md5_file is not None or infile_spec.md5_ref is not None:
            command.append(md5_command(streamed_file=Path(infile_spec.file.name),
                                       out_md5=infile_spec.md5_file,
                                       in_md5_expected=infile_spec.md5_ref))

        unpack_args = {
            "infile": Path("/dev/stdin"),
            "md5_ref": infile_spec.md5_ref,
            "read_number_suffix": infile_spec.read_number_suffix,
            "read_count": infile_spec.read_count,
            "comment_format": infile_spec.comment_format
        }

        # Note that the commands unpack from outer to inner packer.
        command.append(
            infile_spec.formats[-1].unpack_command(**unpack_args)
        )

        for frmt in reversed(infile_spec.formats[0:-1]):
            command.append(frmt.unpack_command(**unpack_args))
        return command


def pack_command(outfile_spec: OutputFileSpec) -> List[str]:
    """
    Input of the command is to come from standard input.
    """
    if outfile_spec.formats is None:
        raise RuntimeError("Oops")
    else:
        if outfile_spec.file is None or outfile_spec.md5_file is None:
            raise RuntimeError("Bugs. Outfile names should be set by now.")
        else:
            pack_args = {
                "infile": Path("/dev/stdin"),
                "outfile": Path("/dev/stdout"),
                "read_count": outfile_spec.read_count,
                "read_number": outfile_spec.read_number,
                "sam_attributes": outfile_spec.sam_attributes,
                "sam_headers": outfile_spec.sam_headers,
                "max_barcodes": outfile_spec.max_barcodes,
                "comment_format": outfile_spec.comment_format
            }

            command = []
            for frmt in outfile_spec.formats[0:-1]:
                command.append(frmt.pack_command(**pack_args))
            command.append(outfile_spec.formats[-1].pack_command(**pack_args))
            command.append(md5_command(streamed_file=Path(outfile_spec.file.name),
                                       out_md5=outfile_spec.md5_file) +
                           f" > {q(str(outfile_spec.file))}")
            return command


def conversion_command(map_spec: MapSpec) -> str:
    command_chain: List[str] = (
            unpack_command(map_spec.input) +
            pack_command(map_spec.output))
    return "set -o pipefail -o errexit -o errtrace; " + \
        " | ".join(command_chain)


def main(map_spec: MapSpec):
    full_command = conversion_command(map_spec)

    print(f"Executing: [{full_command}]", file=sys.stderr)
    subprocess.run(full_command,
                   stderr=None,
                   stdout=None,
                   check=True,
                   shell=True,
                   executable="bash")


def parse_args(args: List[str]):
    parser = argparse.ArgumentParser(description=    # noqa: E251  // better layout
                                     "Convert sequence file from some input format to some output "
                                     "format.\n"
                                     "\n"
                                     "For every input file you can let the script derive "
                                     "the format from the file-suffixes. If that is not possible, "
                                     "you can explicitly set the input and/or output formats. "
                                     "Explicit setting overrides format derivation. "
                                     "\n"
                                     "For the input file we always calculate an MD5 file with the "
                                     "same name as the output file but with '.source_md5' "
                                     "attached.\n"
                                     "\n"
                                     "If you provide an output file name, this filename is used.\n"
                                     "\n"
                                     "If no output file is given for a file, the output file name "
                                     "will be derived from the input file name including its "
                                     "suffixes, that are derived from the output format. "
                                     "If an output file name is given, then no attempt is made "
                                     "to remove existing suffixes and suffixes are not attached. "
                                     "The filename is just as as you provided it.\n"
                                     "\n"
                                     "For the output file there will always be an MD5 file "
                                     "that is placed besides the output file and has the same name "
                                     "as the output file with '.md5' attached.\n"
                                     "\n"
                                     "The format specifications have to be a comma-separated list "
                                     "of allowed format names (" +
                                     ', '.join([f.name for f in list(Format)]) +
                                     ").\n"
                                     "\n"
                                     "It is your responsibility to ensure that output directory, "
                                     "e.g., if your output filename is a relative path with "
                                     "subdirectories.\n")
    parser.add_argument("-i", "--infile",
                        dest="infile",
                        type=Path,
                        action="store",
                        default=Path("/dev/stdin"),
                        help="Input file.")
    parser.add_argument("-I", "--infile-format",
                        dest="infile_format",
                        type=parse_format_spec,
                        action="store",
                        help="Input file format. By default the input file format is "
                        "derived from the input file suffixes. Comma-separated list of: " +
                        ", ".join([p.name for p in Format]))
    parser.add_argument("-X", "--in-md5-ref-file",
                        dest="in_md5_ref_file",
                        type=Path,
                        action="store",
                        default=None,
                        help="An optional file containing an MD5 checksum. " +
                        "The input's MD5 will be checked against this.")
    parser.add_argument("-5", "--in-md5-ref",
                        dest="in_md5_ref_string",
                        action="store",
                        default=None,
                        help="An optional MD5 checksum. Provide only either --in-md5-ref-file or "
                        "or --in-md5-ref as options.")
    parser.add_argument("-x", "--in-md5-file",
                        dest="in_md5_file",
                        type=Path,
                        action="store",
                        default=None,
                        help="Prospective MD5 file for the infile. Will be created. Optional.")
    parser.add_argument("-o", "--outfile",
                        dest="outfile",
                        type=Path,
                        default=Path("/dev/stdout"),
                        action="store",
                        help="Output file.")
    parser.add_argument("-O", "--outfile-format",
                        dest="outfile_format",
                        type=parse_format_spec,
                        action="store",
                        help="Output file format. By default the output format is derived "
                        "from the output filename suffixes.")
    parser.add_argument("-y", "--out-md5-file",
                        dest="out_md5_file",
                        type=Path,
                        action="store",
                        help="Prospective MD5 file for the outfile. Will be created.")
    parser.add_argument("-H", "--sam-header",
                        dest="sam_headers",
                        action="append",
                        help="SAM header line, e.g. for read groups (`@RG`). See SAM "
                        "documentation. Only valid for SAM output formats. Can be provided "
                        "zero to multiple times.")
    parser.add_argument("-a", "--sam-attributes",
                        dest="sam_attributes",
                        action="append",
                        help="SAM attributes to add to each SAM entry. See SAM "
                        "documentation. Only valid for SAM output formats. Can be provided "
                        "zero to multiple times.")
    parser.add_argument("-n", "--read-number",
                        dest="read_number",
                        action="store",
                        default=None,
                        type=int,
                        help="Force the reads to be paired and to be read 1 or read 2. This "
                        "overrides any read number from the identifier suffices or Casava 1.8 "
                        "comments or SAM files. Optional.")
    parser.add_argument("-r", "--read-number-suffix",
                        dest="read_number_suffix",
                        action="store_true",
                        help="If set with FASTQ output, add /1 or /2 to paired end read "
                        "identifiers.")
    parser.add_argument("-C", "--read-count",
                        dest="read_count",
                        action="store",
                        type=int,
                        help="The expected number of reads in the input. The count is checked "
                        "and the program fails, if the observed number doesn't match the expected "
                        "number")
    parser.add_argument("-k", "--comment-format",
                        dest="comment_format",
                        action="store",
                        default=DEFAULT_COMMENT_FORMAT,
                        type=CommentFormat.from_string,
                        help="Whether to write FASTQ comment-related information ('illumina') "
                        f"or treat it opaquely ('opaque'). Default '{DEFAULT_COMMENT_FORMAT}'. "
                        "For 'illumina' and "
                        "FASTQ input the comment will be assumed to be Casava 1.8 and parsed. "
                        "For SAM input, the information will be retrieved from flags and tags. "
                        "If the information cannot be retrieved the following defaults will be "
                        "used in the output: 0 = control bits, '' barcode. "
                        "In both cases, the read number will be taken from the --read-number "
                        "parameter, the /1 and /2 identifier suffixes, the Casava 1.8. comments "
                        "(if the input FASTQ uses Casava 1.8 comments), or the SAM flags (for SAM "
                        "format) (from highest to lowest priority)."
                        "If set to 'opaque' the comment will no be parsed, and encoded in SAM "
                        "as CO:Z:$comment tags. The read number information will then be taken "
                        "from the --read-number parameter or the /1 and /2 identifier suffixes "
                        "(in that priority). If the input is SAM with CO:Z tags and the output "
                        "is FASTQ, then FASTQ comments will be generated from the CO tags.")
    parser.add_argument("-b", "--max-barcodes",
                        dest="max_barcodes",
                        default=None,
                        type=int,
                        action="store",
                        help="Maximum number of different barcodes expected in output. "
                        "Currently, only works for SAM output. By default not checked.")
    return parser.parse_args(args)


if __name__ == "__main__":
    limit_heap_size(20, 30)
    args = parse_args(sys.argv[1:])

    map_spec = to_map_spec(args)

    main(map_spec)
