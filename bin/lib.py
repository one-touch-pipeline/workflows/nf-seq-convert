# SPDX-FileCopyrightText: 2023 The OTP authors
#
# SPDX-License-Identifier: MIT
from __future__ import annotations

import re
import resource
import sys
import typing
from abc import ABCMeta, abstractmethod
from enum import Enum
from typing import Optional, TypeVar, Generic, List, Self, cast


class AbstractComment(metaclass=ABCMeta):

    def __init__(self,
                 read_number: Optional[int] = None,
                 is_filtered: Optional[bool] = None,
                 control_number: Optional[int] = None,
                 barcode_sequence: Optional[str] = None):
        """
        Use the cls.create(...) class method for creating instances.
        """
        self._read_number = read_number
        self._is_filtered = is_filtered
        self._control_number = control_number
        self._barcode_sequence = barcode_sequence

    @classmethod
    @abstractmethod
    def create(cls, *args, **kwargs) -> Self:
        pass

    # It may seem counter-intuitive to have default methods returning None, but None is actually
    # a valid value that means that the requested information was not encoded in or could not get
    # derived from the comment text.
    @property
    def read_number(self) -> Optional[int]:
        return None

    @read_number.setter
    def read_number(self, read_number: int) -> None:
        pass

    @property
    def is_filtered(self) -> Optional[bool]:
        return None

    @property
    def control_number(self) -> Optional[int]:
        return None

    @property
    def barcode_sequence(self) -> Optional[str]:
        return None

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({str(self)})"


class OpaqueComment(AbstractComment):

    def __init__(self, value: str):
        super().__init__()
        self._value = value

    @classmethod
    def create(cls, value: str) -> Self:
        return cls(value)

    def __str__(self) -> str:
        return self._value

    def __eq__(self, other) -> bool:
        return type(self) is type(other) \
            and str(self._value) == str(other._value)


class IlluminaComment(AbstractComment):

    def __init__(self,
                 read_number: int = 1,
                 is_filtered: bool = False,
                 control_number: int = 0,
                 barcode_sequence: str = ""):
        super().__init__(read_number=read_number,
                         is_filtered=is_filtered,
                         control_number=control_number,
                         barcode_sequence=barcode_sequence)

    def __eq__(self, other) -> bool:
        return (type(self) is type(other)
                and self.read_number == other.read_number
                and self.is_filtered == other.is_filtered
                and self.control_number == other.control_number
                and self.barcode_sequence == other.barcode_sequence)

    @classmethod
    def create(cls, value: str) -> Self:
        return cls.from_illumina_fastq_illumina(value)

    @classmethod
    def from_illumina_fastq_illumina(cls, value: str) -> Self:
        """
        See page 22 in:

        https://emea.support.illumina.com/content/dam/illumina-support/documents/documentation/software_documentation/bcl2fastq/bcl2fastq_letterbooklet_15038058brpmi.pdf
        """
        match = re.match(r"^(\d+):([NY]):(\d+):(\S*)$", value)
        if match is None:
            raise RuntimeError(f"Could not parse Illumina comment: '{value}'")
        yn_map = {
            "Y": True,
            "N": False
        }
        return cls(read_number=int(match.groups()[0]),
                   is_filtered=yn_map[match.groups()[1].upper()],
                   control_number=int(match.groups()[2]),
                   barcode_sequence=match.groups()[3])

    @property
    @typing.no_type_check
    def read_number(self) -> int:
        # Note that the return type is narrowed, but mypy shows a type error. The value is
        # taken from the field of the general type (Optional[int]) as derived from the super-
        # class constructor. The assertion fixes the mypy error for the other getters, but not
        # for this one (for unknown reasons). The asserted fact is always true, though (enforced
        # by type annotation on IlluminaComment.__init__ and setter checks. Still, here we have
        # to turn off type checking on the method.
        assert self._read_number is not None
        return cast(int, self._read_number)

    @read_number.setter
    def read_number(self, read_number: int) -> None:
        if read_number not in [1, 2]:
            raise ValueError(f"Invalid read number for IlluminaComment (not 1 or 2): {read_number}")
        self._read_number = read_number

    @property
    def is_filtered(self) -> bool:
        assert self._is_filtered is not None
        return self._is_filtered

    @property
    def control_number(self) -> int:
        assert self._control_number is not None
        return self._control_number

    @property
    def barcode_sequence(self) -> str:
        assert self._barcode_sequence is not None
        return self._barcode_sequence

    def __str__(self) -> str:
        return "%s:%s:%s:%s" % (
            self.read_number,
            'Y' if self.is_filtered else 'N',
            self.control_number,
            self.barcode_sequence
        )


C = TypeVar("C", bound="AbstractComment", covariant=True)


TAG_NAME_RE = r"[A-Za-z][A-Za-z0-9]"


def assert_tag_name(name: str, value: str):
    """
    Check that the `value` consists of two letters or numbers, like specified in the
    SAMv1 format specification. Throw ValueError if not, using the `name` in the message.
    """
    if not re.match(TAG_NAME_RE, value):
        raise ValueError(f"Invalid {name} value: '{value}'")


class SamFlag:
    PAIRED = 0x1
    READ_UNMAPPED = 0x4
    MATE_UNMAPPED = 0x8
    FIRST_IN_PAIR = 0x40
    SECOND_IN_PAIR = 0x80
    IS_FILTERED = 0x200


class CommentFormat(Enum):
    ILLUMINA = "illumina"
    OPAQUE = "opaque"

    @staticmethod
    def from_string(value: str) -> CommentFormat:
        return CommentFormat[value.upper()]


class SeqEntry(Generic[C]):

    def __init__(self,
                 identifier: str,
                 sequence: str,
                 qualities: str,
                 paired: Optional[bool] = None,
                 read_number: Optional[int] = None,
                 comment: Optional[C] = None):
        self._identifier = identifier
        self._sequence = sequence
        self._qualities = qualities
        self._paired = paired
        self.read_number = read_number
        self._comment = comment

    def __eq__(self, other) -> bool:
        if type(self) is type(other):
            return self._identifier == other._identifier \
                and self._sequence == other._sequence \
                and self._qualities == other._qualities \
                and self._read_number == other._read_number \
                and self._comment == other._comment \
                and self._paired == other._paired
        else:
            return False

    @property
    def identifier(self) -> str:
        return self._identifier

    @property
    def sequence(self) -> str:
        return self._sequence

    @property
    def qualities(self) -> str:
        return self._qualities

    @property
    def read_number(self) -> Optional[int]:
        """
        If the read is marked paired, use the read-number from the sequence, if it is set,
        or fall back to the read number from the Illumina-comment. If neither is set, it
        might still be the read is paired, but that information has not been given to the script.
        In that case return None. The information may be, e.g. encoded in the filename.

        If the read is not marked paired, but still has a read number associated, this is clearly
        a bug. Throw a RuntimeError.
        """
        if self._paired:
            if self._read_number is not None:
                return self._read_number
            elif isinstance(self._comment, IlluminaComment):
                return self._comment.read_number
            else:
                return None
        else:
            if self._read_number is not None:
                # The read is not marked paired, but there is a read number! Sounds like an error.
                raise RuntimeError(f"Read {self.identifier} is not marked paired, but has read "
                                   f"number {self._read_number}.")
            else:
                return self._read_number

    @read_number.setter
    def read_number(self, read_number: Optional[int]) -> None:
        """
        Setting the read number to a value != None also marks the read as paired.
        Note that setting a read number on the sequence does not check the read number on the
        comment. So there is a chance that setting the read number creates an inconsistency.
        """
        if read_number not in [1, 2, None]:
            raise ValueError(f"Invalid paired-end read number: {read_number}")
        self._paired = read_number is not None
        self._read_number = read_number

    @property
    def paired(self) -> Optional[bool]:
        """
        Returns none, if the read identifier did not contain information abut the pairing status,
        and the status was not implicitly set by setting the read number.
        """
        return self._paired

    @property
    def comment(self) -> Optional[C]:
        return self._comment

    def to_fastq(self, write_read_number_suffix: bool = False) -> str:
        header = "@" + self.identifier
        if write_read_number_suffix and self.paired:
            header += "/" + str(self.read_number)
        if self.comment is not None:
            header += " " + str(self.comment)
        return f"{header}\n{self.sequence}\n+\n{self.qualities}\n"

    def to_sam_line(self,
                    comment_format: CommentFormat = CommentFormat.ILLUMINA,
                    static_tags: Optional[List[str]] = None,
                    opaque_comment_tag: str = "XF",
                    barcode_tag: str = "BC",
                    control_bits_tag: str = "XC") -> str:
        """
        The comment_format determines, whether detailed information are written into dedicated
        tags ore whether the comment associated with the sequence will be handled opaquely and
        written to a single dedicated tag.
        """
        if static_tags is None:
            static_tags = []
        flags: int = SamFlag.READ_UNMAPPED
        if self.paired:
            flags |= SamFlag.PAIRED | SamFlag.MATE_UNMAPPED
            if self.read_number == 1:
                flags |= SamFlag.FIRST_IN_PAIR
            else:
                flags |= SamFlag.SECOND_IN_PAIR

        comment_derived_tags = []
        if self.comment is not None:
            if self.comment.is_filtered is not None and self.comment.is_filtered:
                flags |= SamFlag.IS_FILTERED
            if comment_format == CommentFormat.OPAQUE:
                comment_derived_tags += [f"{opaque_comment_tag}:Z:{self.comment}"]
            elif comment_format == CommentFormat.ILLUMINA:
                comment_derived_tags += [f"{barcode_tag}:Z:{self.comment.barcode_sequence}"]
                comment_derived_tags += [f"{control_bits_tag}:i:{self.comment.control_number}"]
            else:
                raise RuntimeError(f"Unknown comment format '{comment_format}")

        return "\t".join([self.identifier,
                          str(flags), "*", "0", "0", "*", "*", "0", "0",
                          self.sequence,
                          self.qualities] +
                         comment_derived_tags + static_tags
                         ) + "\n"

    def __repr__(self) -> str:
        return self.to_fastq()


def limit_heap_size(soft_mb: int, hard_mb: int) -> None:
    soft_heap, hard_heap = resource.getrlimit(resource.RLIMIT_DATA)
    new_soft_heap = soft_mb * 1024 * 1024
    new_hard_heap = hard_mb * 1024 * 1024
    print("Changing heapsize memory limits: " +
          f"soft={soft_heap} b -> {new_soft_heap} b (={soft_mb} mb), " +
          f"hard={hard_heap} b -> {new_hard_heap} b (={hard_mb} mb)",
          file=sys.stderr)
    resource.setrlimit(resource.RLIMIT_DATA, (new_soft_heap, new_hard_heap))
