#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 The OTP authors
#
# SPDX-License-Identifier: MIT

import argparse
import re
import shlex
import sys
from pathlib import Path
from textwrap import dedent
from typing import Optional, TextIO, List, Tuple, Dict, Callable

from lib import (
    OpaqueComment, SeqEntry, CommentFormat, IlluminaComment, C, AbstractComment, limit_heap_size)


HEADER_RE = r"^@(\S+?)(?:/(\d+))?(?:\s+(.*))?$"


def parse_fastq_header(line: str,
                       comment_constructor: Callable[[str], C] =
                       OpaqueComment.create  # type: ignore
                       # The type declaration should be correct, but mypy still calls it an error?
                       ) \
        -> Tuple[str, Optional[int], Optional[C]]:
    header_match = re.match(HEADER_RE, line)

    if header_match is None:
        raise RuntimeError(f"Couldn't parse FASTQ entry header: '{line}'")

    identifier = header_match.groups()[0]
    read_number = int(header_match.groups()[1]) \
        if header_match.groups()[1] is not None else None
    comment = comment_constructor(header_match.groups()[2]) \
        if header_match.groups()[2] is not None else None

    if read_number is not None and read_number not in [1, 2]:
        raise RuntimeError(f"Invalid read number: {read_number}")

    return identifier, read_number, comment


def parse_fastq_entry(handle: TextIO,
                      comment_constructor: Callable[[str], C] = OpaqueComment.create  # type: ignore
                      # The type declaration should be correct, but mypy still calls it an error?
                      ) -> Optional[SeqEntry[C]]:
    """
    Parse a full (4 line) FASTQ entry. The comment is not decomposed further. The '+' line is
    ignored.

    Returns None if the end of file is reached.
    """
    header = handle.readline().rstrip("\n")
    sequence = handle.readline().rstrip("\n")
    handle.readline()
    qualities = handle.readline().rstrip("\n")
    if header == "":
        return None
    else:
        identifier, read_number, comment = parse_fastq_header(header, comment_constructor)
        if len(sequence) != len(qualities):
            raise ValueError(f"Possibly corrupt FASTQ {identifier}/{read_number}:\n" +
                             "\t" + sequence + "\n" +
                             "\t" + qualities + "\n")
        return SeqEntry(identifier=identifier,
                        sequence=sequence,
                        qualities=qualities,
                        read_number=read_number,
                        comment=comment)


def parse_args(args: List[str]):
    parser = argparse.ArgumentParser(
        description="Read in a FASTQ file and return a SAM (ASCII, a.k.a. TAM) formatted file.",
        epilog=dedent("""
        ## Flags: Paired-end FASTQ

        If reads are marked with /1 or /2 it is considered paired-end and the following SAM flags
        are set:

         * 1: read is paired
         * 4: read is unmapped
         * 8: mate is unmapped
         * 64: read 1 (or (not 64) for read 2)

        ## Flags: Single-end FASTQ

        If a read has no /1 or /2 suffix on its ID, then the read is considered single-end and the
        following flags are set:

         * 4: read is unmapped

        # Read Groups & Barcodes

        The script cannot create read-groups automatically for each read. Thus,
        `--header "@RG\t1:...`"` will usually be used together with `--max-barcodes 1` and
        `--attribute RG:Z:1`, i.e. for the case of a single read-group for the FASTQ set.
        """)
    )
    parser.add_argument("-i", "--fastq-in",
                        default=Path("/dev/stdin"),
                        dest="fastq",
                        action="store",
                        type=Path,
                        help="Input FASTQ file. Defaults to standard input.")
    parser.add_argument("-o", "--sam-out",
                        default=Path("/dev/stdout"),
                        dest="sam",
                        action="store",
                        type=Path,
                        help="Output SAM file. Defaults to standard output.")
    parser.add_argument("-H", "--header",
                        type=str,
                        dest="headers",
                        action="append",
                        default=[],
                        help="SAM header lines. Usually you put in here the @RG lines. Empty "
                        "strings are ignored. Can be provided zero to multiple times.")
    parser.add_argument("-a", "--attribute",
                        type=str,
                        action="append",
                        dest="attributes",
                        default=[],
                        help="Static SAM attribute. E.g. if you set a @RG line in the header you "
                        "may want to set 'RG:Z:...' for every read, to make sure the read is "
                        "associated with that read-group. Empty strings are ignored Can be "
                        "provided zero to multiple times.")
    parser.add_argument("-n", "--read-number",
                        type=int,
                        action="store",
                        dest="read_number",
                        help="Whether the input file is read 1 or 2 paired end reads. Use this to "
                        "statically add (ORed) flag 77 for first reads and 141 for second "
                        "reads, if the input file lacks /1 and /2 for paired end data.")
    parser.add_argument("-C", "--read-count",
                        action="store",
                        dest="read_count",
                        default=None,
                        type=int,
                        help="Optionally, check that the input has the given number of reads.")
    parser.add_argument("-b", "--max-barcodes",
                        type=int,
                        action="store",
                        dest="max_barcodes",
                        help="For the special case of Illumina-formatted comments, check that no "
                        "more than the specified number of different barcodes can be parsed from "
                        "the FASTQ entry comments. Defaults: Do not check.")
    parser.add_argument("-k", "--comment-format",
                        type=CommentFormat.from_string,
                        action="store",
                        dest="comment_format",
                        default=CommentFormat.ILLUMINA,
                        help="If 'opaque', FASTQ comments treated opaquely and are simply mapped "
                             "to XF:Z tags in the SAM output. "
                             "If 'illumina' then FASTQ comments are assumed to be Illumina-"
                             "formatted (Casava 1.8), i.e. $readNumber:$filtered:$controlNumber:"
                             "$barcode. The information will be parsed and encoded in the SAM "
                             "output file as flags (filtered, readNumber) or tags (BC:Z for "
                             "barcode, and custom tag XC:i for controlNumber). If the input "
                             "doesn't have comments, the read-number information is taken from "
                             "the --read-number parameter or the /1 and /2 identifier suffixes, "
                             "or the reads are assumed to be unpaired, while the control bit is "
                             "assumed to be 0.")
    return parser.parse_args(args)


def print_pg_header(command: Optional[List[str]], file: TextIO) -> None:
    """
    Header lines contain tabulators and possible '@'s which confuse the samtools header when
    printed in the @PG line. We need to quote them when printing the command in the @PG line.
    """
    pg_line = ["@PG", "ID:fastqToSam.py",
               "PN:fastqToSam.py"]

    if command is not None:
        indices_to_quote = [
            idx + 1
            for idx in range(0, len(command))
            if command[idx] in ["--header", "-h"]
        ]

        quoted_command = [
            # Quote all shell specials plus replace literal tabs by escape-sequence-tabs.
            "$" + re.sub(r'\t', '\\\\t', shlex.quote(command[idx]))
            if idx in indices_to_quote
            else command[idx]
            for idx in range(0, len(command))
        ]

        pg_line.append(f"CL:{' '.join(quoted_command)}")

    print("\t".join(pg_line),
          file=file)


def maybe_override_read_number(entry: SeqEntry,
                               read_number: Optional[int]) -> bool:
    """
    If the read_number is not None, override the read number in the entry.
    If the entry was changed from 1->2 or vice versa, return True, else False.
    """
    was_modified = False
    if read_number is not None:
        if (entry.read_number is not None and
                entry.read_number != read_number):
            was_modified = True
        entry.read_number = read_number
        if entry.comment is not None:
            # Override the read number in the Illumina comment. For opaque comments,
            # the read_number field will be ignored.
            entry.comment.read_number = read_number
    return was_modified


def get_comment_constructor(comment_format: CommentFormat) \
        -> Callable[[str], AbstractComment]:
    comment_constructor: Callable[[str], AbstractComment]
    if comment_format == CommentFormat.OPAQUE:
        comment_constructor = OpaqueComment.create
    elif comment_format == CommentFormat.ILLUMINA:
        comment_constructor = IlluminaComment.create
    else:
        raise RuntimeError("Oops! Bug!")
    return comment_constructor


def main(infile_handle: TextIO,
         outfile_handle: TextIO,
         headers: List[str],
         comment_format: CommentFormat,
         attributes: List[str],
         read_number: Optional[int],
         expected_read_count: Optional[int],
         max_barcodes: Optional[int],
         command: Optional[List[str]] = None) -> None:

    print_pg_header(command, file=outfile_handle)
    for header_line in headers:
        print(header_line, file=outfile_handle)

    observed_barcodes: Dict[str, int] = {}
    eof_reached = False
    already_warned = False
    comment_constructor = get_comment_constructor(comment_format)
    processed_reads = 0
    while not eof_reached:
        switched_read_number = False

        fq_entry: Optional[SeqEntry[AbstractComment]] = \
            parse_fastq_entry(infile_handle, comment_constructor)
        if fq_entry is None:
            eof_reached = True

        else:
            if max_barcodes is not None:
                if fq_entry.comment is not None and fq_entry.comment.barcode_sequence is not None:
                    observed_barcodes[fq_entry.comment.barcode_sequence] = \
                        observed_barcodes.get(fq_entry.comment.barcode_sequence, 0) + 1
                switched_read_number = maybe_override_read_number(fq_entry, read_number)
                outfile_handle.write(fq_entry.
                                     to_sam_line(comment_format=comment_format,
                                                 static_tags=attributes))
            else:
                switched_read_number = maybe_override_read_number(fq_entry, read_number)
                outfile_handle.write(fq_entry.
                                     to_sam_line(comment_format=comment_format,
                                                 static_tags=attributes))
            processed_reads += 1

        if switched_read_number and not already_warned:
            print("WARNING: Overriding read number from %s to %s" %
                  (read_number % 2 + 1 if read_number is not None else None, read_number),
                  # 1->2 or 2->1, just calculate the original
                  file=sys.stderr)

    if max_barcodes is not None:
        observed_barcode_num = len(observed_barcodes.keys())
        if observed_barcode_num > max_barcodes:
            msg = f"Number of observed barcodes {observed_barcode_num} is larger than " + \
                  f"requested maximum {max_barcodes}! {observed_barcodes}"
            raise RuntimeError(msg)

    if expected_read_count is not None and expected_read_count != processed_reads:
        raise ValueError(f"Processed {processed_reads} instead of expected "
                         f"{expected_read_count} reads.")

    print(f"fastqToSam.py processed {processed_reads} reads.", file=sys.stderr)


if __name__ == "__main__":
    limit_heap_size(20, 30)
    args = parse_args(sys.argv[1:])

    with (open(args.fastq, "r") if args.fastq != Path("/dev/stdin") else sys.stdin) \
            as infile_handle:
        with (open(args.sam, "w") if args.sam != Path("/dev/stdout") else sys.stdout) \
                as outfile_handle:
            command = [str(Path(sys.argv[0]).name)] + sys.argv[1:]
            main(infile_handle,
                 outfile_handle,
                 [h for h in args.headers if h != ""],
                 args.comment_format,
                 [a for a in args.attributes if a != ""],
                 args.read_number,
                 args.read_count,
                 args.max_barcodes,
                 command)
