// SPDX-FileCopyrightText: 2023 The OTP authors
//
// SPDX-License-Identifier: MIT

package de.dkfz.odcf.nfSeqConvert

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.networknt.schema.JsonSchema
import com.networknt.schema.JsonSchemaFactory
import com.networknt.schema.SpecVersion
import com.networknt.schema.ValidationMessage
import groovy.transform.CompileStatic

@CompileStatic
class ConfigValidation {

    final public ObjectMapper mapper = new ObjectMapper().
            enable(DeserializationFeature.FAIL_ON_TRAILING_TOKENS)

    final private JsonSchema schema =
            getJsonSchemaFromClasspath("de/dkfz/odcf/nfSeqConvert/config.schema.json")

    static protected JsonSchema getJsonSchemaFromClasspath(String name) {
        JsonSchemaFactory factory = JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V4)
        InputStream inputStream = this.classLoader.getResourceAsStream(name)
        return factory.getSchema(inputStream)
    }

    protected JsonNode getJsonNodeFromStringContent(String content) throws IOException {
        return mapper.readTree(content);
    }

    Set<ValidationMessage> validate(JsonNode config) {
        return schema.validate(config)
    }

    Set<ValidationMessage> validate(String config) {
        return validate(getJsonNodeFromStringContent(config))
    }

    public <K, V> Set<ValidationMessage> validate(Map<K, V> config) {
        return validate(mapper.convertValue(config, JsonNode.class))
    }

    public <K, V> String prettyPrint(Map<K, V> config) {
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(
                mapper.convertValue(config, JsonNode.class))
    }

}
