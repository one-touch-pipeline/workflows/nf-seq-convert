// SPDX-FileCopyrightText: 2023 The OTP authors
//
// SPDX-License-Identifier: MIT

package de.dkfz.odcf.nfSeqConvert

import org.apache.commons.text.translate.CharSequenceTranslator
import org.apache.commons.text.translate.LookupTranslator

class Escape {

    /** Compare
     *  https://commons.apache.org/sandbox/commons-text/jacoco/org.apache.commons.text/StringEscapeUtils.java.html
     */
    static final CharSequenceTranslator ESCAPE_BASH =
            new LookupTranslator(
                [
                    // XSI standard. Copied from above link, line 296+
                    "|": "\\|",
                    "&": "\\&",
                    ";": "\\;",
                    "<": "\\<",
                    ">": "\\>",
                    "(": "\\(",
                    ")": "\\)",
                    "\$": "\\\$",
                    "`": "\\`",
                    "\\": "\\\\",
                    "\"": "\\\"",
                    "'": "\\'",
                    " ": "\\ ",
                    "\t": "\\\t",
//                    "\r\n": "",    // Don't make sense here. See below.
//                    "\n": "",
                    "*": "\\*",
                    "?": "\\?",
                    "[": "\\[",
                    "#": "\\#",
                    "~": "\\~",
                    "=": "\\=",
                    "%": "\\%",

                    // Bash additions
                    "}": "\\}",
                    "{": "\\}",
                    "!": "\\!",
                    "\f": "\\\f",
                    "\b": "\\\b",

                    // Deviations from XSI
                    "\r\n": "\\r\\n",
                    "\n": "\\n",
                ] as HashMap<CharSequence, CharSequence>)

    static String escapeBash(String string) {
        return ESCAPE_BASH.translate(string)
    }

}
