// SPDX-FileCopyrightText: 2023 The OTP authors
//
// SPDX-License-Identifier: MIT

package de.dkfz.odcf.nfSeqConvert

import spock.lang.Specification

class EscapeTest extends Specification {

    def testEscapeBash(String input, String expected) {
        setup:
        String escaped = Escape.escapeBash(input)

        expect:
        expected == escaped

        where:
        input  | expected
        // Bash additions
        "}"    | "\\}"
        "{"    | "\\}"
        "!"    | "\\!"
        "\f"   | "\\\f"
        "\b"   | "\\\b"

        // Deviations from XSI
        "\r\n" | "\\r\\n"
        "\n"   | "\\n"

    }
}
