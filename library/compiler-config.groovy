// SPDX-FileCopyrightText: 2023 The OTP authors
//
// SPDX-License-Identifier: MIT

import groovy.transform.TypeChecked

// https://docs.groovy-lang.org/latest/html/documentation/#_configscript_example_static_compilation_by_default
withConfig(configuration) {
    ast(TypeChecked)
}
